
var FancyWebSocket = function (url)
{
    var ruta = "";
    if ($("#fancyurl").val() == "1") {
        ruta = "../";
    }
    //var urlbase = "http://192.168.0.152/laravelcei/public/";
    var callbacks = {};
    var ws_url = url;
    var conn;

    this.bind = function (event_name, callback)
    {
        callbacks[event_name] = callbacks[event_name] || [];
        callbacks[event_name].push(callback);
        return this;
    };

    this.send = function (event_name, event_data)
    {
        this.conn.send(event_data);
        return this;
    };

    this.connect = function ()
    {
        if (typeof (MozWebSocket) == 'function')
            this.conn = new MozWebSocket(url);
        else
            this.conn = new WebSocket(url);

        this.conn.onmessage = function (evt)
        {
            dispatch('message', evt.data);
        };

        this.conn.onclose = function () {
            dispatch('close', null)
        }
        this.conn.onopen = function () {
            dispatch('open', null)
        }
    };

    this.disconnect = function ()
    {
        this.conn.close();
    };

    var dispatch = function (event_name, message)
    {
        if (message == null || message == "")//aqui es donde se realiza toda la accion
        {

            //actualiza_mensaje(message);
        }
        else
        {
            //alert('llego aqui');                         
            //var JSONdata    = JSON.parse(message); //parseo la informacion
            var type_event = message.split(',');
            switch (type_event[0]) {
                case "nuevochat":

                    var cont = $('#chats');
                    var list = $('.chats', cont);
                    var id_unico_loguin = $("#id_unico_loguin").val();

                    if (id_unico_loguin == type_event[1]) {



                        $.ajax({
                            type: "GET",
                            url: "newmessage",
                            cache: false,
                            data: {id: type_event[2]},
                            async: false
                        }).done(function (html) {

                            var nuevomsj2 = '<li class="in">\n\
                                        <img class="avatar img-responsive" alt="" src="' + type_event[3] + '" />\n\
                                        <div class="message"><span class="arrow"></span><a href="#" class="name">' + type_event[4] + ' </a><span class="datetime"> at ' + html[0] + '</span>\n\
                                        <span class="body">' + html[1] + '</span></div></li>';

                            $("#chat_int_" + html[2]).append(nuevomsj2);
                            $('.scroller', cont).slimScroll({
                                scrollTo: list.height()
                            });

                        });

                    }
                    break;

                case "newuser":
                    actualiza_usuarios(type_event);
                    actualiza_notificaciones(type_event);
                    actualiza_cont(type_event);
                    mostrar_not(type_event);
                    break;

                case "newsupport":

                    var html = $.ajax({
                        type: "GET",
                        url: ruta + "notnewsupport",
                        cache: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cache: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html2);


                    if (html != 0) {

                        /*var html_faq = $.ajax({
                         type: "GET",
                         url: ruta+"support_faq",
                         cache:false,
                         data: {description: type_event[4], category: type_event[5]},
                         async: false
                         }).responseText;*/

                        type_event[4] = html;
                        var mensaje = "Creo un nuevo soporte";
                        var href = 'admin_supports';
                        var f = new Date();
                        var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
                        actualiza_cont(type_event);
                        actualiza_contmenu("sumar", "4");
                        actualiza_notificaciones(ruta, href, mensaje, type_event, fecha);
                        toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' ha creado una nueva solicitud', 'Nueva Solicitud');
                    }
                    break;

                case "asignsupport":

                    var html = $.ajax({
                        type: "GET",
                        url: ruta + "notasignsupport",
                        cahe: false,
                        data: {responsible: type_event[4]},
                        async: false
                    }).responseText;

                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "respond_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_respond_supports').html(html2);


                    var html3 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html3);

                    if (html != 0) {
                        type_event[4] = html;
                        var mensaje = "Le asigno un nuevo soporte";
                        var href = ruta + 'respond_supports';
                        var f = new Date();
                        var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
                        actualiza_cont(type_event);
                        actualiza_contmenu("sumar", "5");
                        actualiza_notificaciones(ruta, href, mensaje, type_event, fecha);
                        toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' le asigno un nuevo soporte', 'Nuevo Soporte');
                    }
                    break;

                case "refuse_support":

                    var html = $.ajax({
                        type: "GET",
                        url: ruta + "notCloseSupport",
                        cahe: false,
                        data: {id_support: type_event[4]},
                        async: false
                    }).responseText;

                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "respond_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_respond_supports').html(html2);


                    var html3 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html3);
                    actualiza_contmenu("restar", "4");

                    if (html != 2) {
                        type_event[4] = html;
                        var mensaje = "Respondio su soporte.";
                        var href = 'my_supports_history';
                        var f = new Date();
                        var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
                        actualiza_cont(type_event);
                        //actualiza_contmenu("restar","4");
                        actualiza_notificaciones(ruta, href, mensaje, type_event, fecha);
                        toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' Cerro su soporte.', 'Soporte cerrado');
                    }
                    break;

                case "cancel_support":

                    var html = $.ajax({
                        type: "GET",
                        url: ruta + "notnewsupport",
                        cahe: false,
                        data: {id_support: type_event[1]},
                        async: false
                    }).responseText;

                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "respond_supports",
                        cahe: false,
                        data: {nombre: type_event[0]},
                        async: false
                    }).responseText;

                    $('#table_respond_supports').html(html2);


                    var html3 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cahe: false,
                        data: {nombre: type_event[0]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html3);

                    if (html != 0) {
                        type_event[4] = html;
                        var mensaje = "Respondio su soporte.";
                        var href = 'my_supports_history';
                        var f = new Date();
                        var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());

                        actualiza_contmenu("restar", "4");

                        toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' Anulo su soporte.', 'Soporte anulado');


                    }
                    break;

                case "acept_support":

                    if (type_event[5] == 2) {

                        var html = $.ajax({
                            type: "GET",
                            url: ruta + "notCloseSupport",
                            cahe: false,
                            data: {id_support: type_event[4]},
                            async: false
                        }).responseText;

                        if (html != 2) {
                            type_event[4] = html;
                            var mensaje = "Su soporte fue cerrado, por favor califiquelo.";
                            var href = 'my_supports_history';
                            var f = new Date();
                            var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
                            actualiza_cont(type_event);
                            //actualiza_contmenu("5");
                            actualiza_notificaciones(ruta, href, mensaje, type_event, fecha);
                            toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' Cerro su soporte.', 'Soporte cerrado');
                        }

                    }



                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "respond_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_respond_supports').html(html2);

                    var html3 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html3);

                    break;

                case "scaled_support":



                    var html = $.ajax({
                        type: "GET",
                        url: ruta + "notScaledSupport",
                        cahe: false,
                        data: {id_support: type_event[4], user_asign: type_event[5]},
                        async: false
                    }).responseText;

                    if (html != 0) {
                        type_event[6] = html;
                        var mensaje = "Le escalo un soporte.";
                        var href = 'respond_supports';
                        var f = new Date();
                        var fecha = ("Hoy a las " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds());
                        actualiza_cont(type_event);
                        actualiza_contmenu("sumar", "5");
                        actualiza_notificaciones_escaled(ruta, href, mensaje, type_event, fecha);
                        toastr.success('<img alt="" class="top-avatar" src="' + ruta + type_event[3] + '"/> </br>El usuario ' + type_event[1] + ' ' + type_event[2] + ' Le escalo un soporte.', 'Soporte Escalado');
                    }


                    var html2 = $.ajax({
                        type: "GET",
                        url: ruta + "respond_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_respond_supports').html(html2);

                    var html3 = $.ajax({
                        type: "GET",
                        url: ruta + "admin_supports",
                        cahe: false,
                        data: {nombre: type_event[1]},
                        async: false
                    }).responseText;

                    $('#table_admin_supports').html(html3);

                    break;




            }

            //aqui se ejecuta toda la accion

        }
    }
};

var Server;
function send(text)
{
    Server.send('message', text);
}
$(document).ready(function ()
{
    Server = new FancyWebSocket('ws://192.168.0.152:12345');
    Server.bind('open', function ()
    {
    });
    Server.bind('close', function (data)
    {
    });
    Server.bind('message', function (payload)
    {
    });
    Server.connect();
});



function actualiza_usuarios(message)
{
    var nuevo = '<tr id="registros" ng-repeat="todo in todos" class="ng-scope">\n\
                            <td class="ng-binding">' + message[1] + '</td>\n\
                            <td class="ng-binding">' + message[2] + '</td>\n\
                            <td class="ng-binding">' + message[3] + '</td>\n\
                            <td class="ng-binding">' + message[4] + '</td>\n\
                            <td class="ng-binding">' + message[5] + '</td><td>\n\
                            <a href="#" class="btn btn-danger delete">\n\
                            <i class="icon-remove"></i></a>\n\
                            <a href="#" class="btn btn-success delete">\n\
                            <i class="icon-edit"></i></a></td></tr>';

    $("#registros2").prepend(nuevo);

}

function actualiza_notificaciones_escaled(ruta, href, message, usuario, fecha)
{
    var id = "'id'";
    var listanot = '<li>\n\
                    <a href="' + href + '" class="notificacion" id="' + usuario[6] + '" onclick="desactive_notification($(this).attr(' + id + '));return false;">\n\
                    <span class="label label-sm label-icon ">\n\
                    <img alt class="top-avatar" src="' + ruta + usuario[3] + '">\n\
                    </span>\n\
                    <strong>' + usuario[1] + ' ' + usuario[2] + ' </strong> ' + message + ' <span class="time"> ' + fecha + '\n\
                    </span>\n\
                    </a>\n\
                    </li>';
    $("#lista-not").prepend(listanot);
}

function actualiza_notificaciones(ruta, href, message, usuario, fecha)
{
    $("#sound").html('<EMBED SRC="' + ruta + 'assets/WAV/Hope.wav" AUTOSTART=TRUE WIDTH=1 HEIGHT=1>')
    document.title = 'Nueva Notificacion';
    var id = "'id'";
    var listanot = '<li>\n\
                    <a href="' + href + '" class="notificacion" id="' + usuario[4] + '" onclick="desactive_notification($(this).attr(' + id + '));return false;">\n\
                    <span class="label label-sm label-icon ">\n\
                    <img alt class="top-avatar" src="' + ruta + usuario[3] + '">\n\
                    </span>\n\
                    <strong>' + usuario[1] + ' ' + usuario[2] + ' </strong> ' + message + ' <span class="time"> ' + fecha + '\n\
                    </span>\n\
                    </a>\n\
                    </li>';
    $("#lista-not").prepend(listanot);
}

function actualiza_cont(message)
{

    var cont = $("#cont-not").html();
    cont = parseInt(cont);
    cont = cont + 1;
    var icono = '<i class="icon-warning-sign"></i><span class="badge badge-danger" id="cont-not">' + cont + '</span>';
    $("#icon_notifications").html(icono);

}

function actualiza_contmenu(operacion, id)
{
    var cont = $("#" + id).html();
    cont = parseInt(cont);
    if (operacion == "sumar") {
        cont = cont + 1;
        $("#" + id).html(cont);
        $("#" + id).css("display", "block");
    } else if (operacion == "restar") {
        cont = cont - 1;
        $("#" + id).html(cont);
        if (cont == 0) {
            $("#" + id).css("display", "none");
        }
    }

}

function mostrar_not(message)
{
    toastr.success('El usuario ' + message[1] + ' ha sido creado', 'Nuevo Usuario')
}