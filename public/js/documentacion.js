$(function () {

    // función para cargar el contenido de una carpeta en el árbol de navegación

    $(document).on('click', '.folder', function (event) {
        
        var root = parseInt($(this).attr('id'));
        dir = $(this).attr('dir');

        if (root)
        {
            id = "#" + root;

            if (!($(this).hasClass('clicked'))) {
                
                    var html = $.ajax({
                        type: "GET",
                        url: "../cargardocumentos",
                        data: {id: root},
                        async: false
                    }).responseText;

                    $("#ad"+dir).append(html);
                    $( this ).addClass('clicked');
                    $( this ).children('i').removeClass('icon-folder-close');
                    $( this ).children('i').addClass('icon-folder-open');
                    
            }else{
                html = "";
                $("#ad"+dir).html(html);
                $( this ).removeClass('clicked');
                $( this ).children('i').removeClass('icon-folder-open');
                $( this ).children('i').addClass('icon-folder-close');
            }

        }

    });

//función para cargar un documento en el navegador de documentos

    $(document).on('click', '.file', function (e) {

        root = $(this).attr('id');
        my_url = $(this).attr('dir');

        if (root)
        {
            $('#docview embed').remove();
            $('#docview').append('<embed id="doc-container" src="' + my_url + '">');

        }

    });


});