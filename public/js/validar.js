toastr.options = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "showDuration": "5000",
    "hideDuration": "5000",
    "timeOut": "5000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}


// var host = "ws://192.168.0.152/laravelcei/public/js/server.php";
// var socket;




//funcion para actualizar la infromacion del usuario
function actualizar(){

    var celular = $("#celular").val();
    var telefono = $("#telefono").val();
    var email = $("#email").val();
    var direccion = $("#direccion").val();    
    
        var html = $.ajax({
            type: "GET",
            url: "actualizarperfil",
            data: { celular: celular, telefono: telefono, email: email, direccion: direccion },
            async: false
        }).responseText;

        if (html == "1")
        {
            toastr.success('Sus datos se han actualizado correctamente', 'Actualizacion');
        }
        else
        {                        
            toastr.error('No ha sido posible actualizar sus datos', 'Error');
        }
}

function tiene_numeros(texto){
    var numeros="0123456789";
    for(i=0; i<texto.length; i++){
        if (numeros.indexOf(texto.charAt(i),0)!=-1){
            return true;
        }
    }
   return false;
}

function tiene_letras(texto){
    var letras="abcdefghyjklmnñopqrstuvwxyz";
    texto = texto.toLowerCase();
    for(i=0; i<texto.length; i++){
        if (letras.indexOf(texto.charAt(i),0)!=-1){
            return true;
        }
    }
    return false;
}
//funcion para cambiar la contraseña del usuario
function cambiarpass(){
    var nueva = $("#nueva").val();
    var renueva = $("#renueva").val();
    var actual = $("#actual").val();    
    if(nueva==renueva){
        if(nueva.length<6 ){
         toastr.warning('La nueva contraseña debe tener al menos 6 caracteres', 'Error');
         return;
        }else{            
            if(!tiene_numeros(nueva) || !tiene_letras(nueva)){
                toastr.warning('La nueva contraseña debe tener numeros y letras', 'Error');
                return;
            }
        }
        var html = $.ajax({
            type: "GET",
            url: "cambiarpass",
            data: { nueva: nueva, renueva: renueva, actual: actual},
            async: false
        }).responseText;

        if (html == "1"){            
            $("#nueva").val("");
            $("#renueva").val("");
            $("#actual").val("");
            toastr.success('La contraseña ha sido actualizada correctamente', 'Actualizacion');            
            
        }else if(html == "2"){
            toastr.warning('La contraseña actual es incorrecta, verifiquela e intente de nuevo', 'Error');
        
        }else{
            toastr.error('No ha sido posible actualizar su contraseña', 'Error');
        }
    }else{
        toastr.warning('las contraseñas no coinciden, intente nuevamente', 'Error');
    }
}
//funcion para actualizar las redes sociales del usuario
function redes(){

    var linked = $("#linked").val();
    var google = $("#google").val();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var skype = $("#skype").val();
    
        var html = $.ajax({
            type: "GET",
            url: "redes",
            data: { linked: linked, google: google, facebook: facebook, twitter: twitter, skype: skype},
            async: false
        }).responseText;

        if (html == "1"){
            toastr.success('Sus redes sociales se han actualizado correctamente', 'Actualizacion');
            
        }else{
            toastr.error('No ha sido posible actualizar sus redes sociales', 'Error');
        }
}
//funcion para asignar soportes a un usuario
function asing_support(){

    var responsible = $("#responsible").val();
    var status      = $("#status").val();
    var priority    = $("#priority").val();
    var category    = $("#category").val();    
    var observation = $("#observation").val(); 
    var support_id  = $("#support_id").val();
    
        
        
        $.ajax({
        type: "GET",
        url: "asing_support",
        data: { responsible: responsible, status: status, priority: priority, category: category, observation: observation, support_id: support_id }
        })
        .done(function( data ) {
            if(data=="2"){
              toastr.error('No ha sido posible asignar el soporte', 'Error');
            }else{

                if (status == 3) {
                    var type_event = ["refuse_support", data["name"],data["last_name"], data["img_min"], support_id];
                    send( type_event);
                    toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
                }else{
                    var type_event = ["asignsupport", data["name"],data["last_name"], data["img_min"], responsible];
                    send( type_event);
                    toastr.success('Se asigno el soporte correctamente', 'Actualizacion');
                }             
                $("#close_modal").click();
            }              
        });

      
}

function attend_support(){       

    if($("#closed").is(':checked')) {  
        var status = 2; 
    } else {  
        var status = 4;
    }  

    if($("#escalar").is(':checked')) { 
        var status = 5; 
        var scale = 1;
        var user_asign = $("#responsible").val(); 
    } else {  
        var scale = 0;
        var user_asign = "";      
    }


    var observation = $("#observation").val();
    var support_id = $("#support_id").val();

     
    
    var html = $.ajax({
        type: "GET",
        url: "attend_support",
        data: { observation: observation, support_id: support_id, status: status, scale: scale, user_asign: user_asign },
        async: false
    }).done(function( datos ) {
    if(datos=="2"){
        toastr.error('No se a podido actualizar el soporte', 'Error');
    }else{
        toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
        $("#close_modal").click();
            
        var cont = $("#5").html();
        cont = parseInt(cont);            
        cont = cont - 1;
        $("#5").html(cont);
        if(cont==0){
            $("#5").css("display", "none");   
        }           

        if (status == 5 ) {
            var type_event = ["scaled_support", datos["name"],datos["last_name"], datos["img_min"], support_id, user_asign]; 
            send( type_event);             
        }else{
            var type_event = ["acept_support", datos["name"],datos["last_name"], datos["img_min"], support_id, status];
            send( type_event);
              
        }
    }              
});

}
function cancel_support(id_support){

    var html = $.ajax({
        type: "GET",
        url: "cancel_support",
        data: { support_id: id_support},
        async: false
    }).done(function( datos ) {
        if(datos=="2"){
            toastr.error('No ha sido posible actualizar el soporte', 'Error');
        }else{
            toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
            var type_event = ["cancel_support", datos['name'],datos['last_name'],datos['img_min'], id_support];
            send( type_event);
            $("#"+id_support).css("display", "none");
        }              
    });

        
}


// function load_more_notifications(){

//     var html = $.ajax({
//         type: "GET",
//         url: "load_more_notifications",
//         data: { more: "1"},
//         async: false
//     }).done(function( datos ) {
                      
//     });

        
// }


function ratingSupport(){       
    
    
    var question_1 = $('input:radio[name=question_1]:checked').val();
    var question_2 = $('input:radio[name=question_2]:checked').val();
    var question_3 = $('input:radio[name=question_3]:checked').val();
    var support_id = $("#support_id").val();    
    var comment = $("#comment").val();

    
    
    opciones_1 = document.getElementsByName("question_1");
    opciones_2 = document.getElementsByName("question_2");
    opciones_3 = document.getElementsByName("question_3");
     
    var seleccionado1 = false;
    for(var i=0; i<opciones_1.length; i++) {    
        if(opciones_1[i].checked) {
            seleccionado1 = true;
            break;
        }
    }
    var seleccionado2 = false;
    for(var i=0; i<opciones_2.length; i++) {    
        if(opciones_2[i].checked) {
            seleccionado2 = true;
            break;
        }
    }
    var seleccionado3 = false;
    for(var i=0; i<opciones_3.length; i++) {    
        if(opciones_3[i].checked) {
            seleccionado3 = true;
            break;
        }
    }
     
    if(!seleccionado1 || !seleccionado2 || !seleccionado3) {
        toastr.error('Por favor califique el soporte', 'Error');
    }else if (comment == "") {
        toastr.error('El campo Observaciones está vacio', 'Error');
    }else{
        var html = $.ajax({
            type: "GET",
            url: "submit_rating_support",
            data: { question_1: question_1, question_2: question_2, question_3: question_3, support_id: support_id, comment: comment},
            async: false
        }).responseText;

        if (html == "1"){             
            toastr.success('La calificación fue enviada correctamente', 'Actualizacion');
            $("#close_modal").click();
            $("#cal"+support_id).html('<td class="td_center"><i class="icon-check"></i></td>');    
        }
        else{
            
            toastr.error('No ha sido posible calificar el soporte', 'Error');
        }
    }
}

$("#resena").mouseenter(function(evento){
   $('#botoneditar').toggle('slow');
});

$("#resena").mouseleave(function(e){
   $('#botoneditar').toggle('slow');
});


function funcion(){
        
    $('.escalar').each(function(){
        var checkbox = $(this);         
        if (checkbox.is(':checked') == true) {
            $('#select_responsible').toggle('slow');
        }else{
            $('#select_responsible').toggle('slow');
        }
    });
         
}



function guardarResena(id){
    var editor = CKEDITOR.instances['editor'].getData();        
    var html = $.ajax({
    type: "GET",
    url: "../resena",
    data: { id: id, editor: editor},
    async: false
    }).responseText;

    if (html == "1"){
        $("#descriptionresena").html(editor);
        $(".close").click();
        toastr.success('Reseña actualizada correctamente', 'Actualizacion');

    }else{
        toastr.error('No ha sido posible actualizar la reseña', 'Error');
    }
}

function crearDir(id, proceso){
    var nombre = $("#carpeta").val();
        
    $.ajax({
    type: "GET",
    url: "../crearDir",
    data: { id: id, nombre: nombre, proceso: proceso},
    })
    .done(function( data ) {
        if(data=="2"){
            toastr.error('No ha sido posible crear la carpeta', 'Error');
        }else{
              
            $(".close").click();
            toastr.success('carpeta creada correctamente', 'Nueva carpeta');
            $('#carpetas').append(new Option(data["name"], data["id"], true, true));
        }
    });

}

function desactive_notification(valorCaja1){    
    var parametros = {
        "valorCaja1": valorCaja1
    };
    var ruta = "";
        if($("#fancyurl").val() == "1"){
            ruta = "../";
        }
    $.ajax({
        data: parametros,
        url:  ruta+'desactive_notification',
        type: 'get',

        success: function(response){
            if(response=="1"){
                var dir = $('#'+valorCaja1).attr('href');
                window.location =ruta+dir;
            }else{
                alert("error");
            }
                //$("#ajax").html(response);
        }
    });
}

function edit_data_user(){

    var profile     = $("#profile").val();
    var status      = $("#status").val();
    var priority    = $("#priority").val();
    var category    = $("#category").val();    
    var observation = $("#observation").val(); 
    var support_id  = $("#support_id").val();
    
    
    $.ajax({
    type: "GET",
    url: "asing_support",
    data: { responsible: responsible, status: status, priority: priority, category: category, observation: observation, support_id: support_id }
    })
    .done(function( data ) {
        if(data=="2"){
            toastr.error('No ha sido posible asignar el soporte', 'Error');
        }else{
            toastr.success('Se asigno el soporte correctamente', 'Actualizacion');
            $("#close_modal").click();
            var type_event = ["asignsupport", data["name"],data["last_name"], data["img_min"], responsible];
            send( type_event);
           
        }              
    });
}

function exportar(){
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();
    var dir = "exportar?inicio="+fecha_inicio+"&fin="+fecha_fin;
    window.open(dir);    
}

function exportar_help_desk(){
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();
    var dir = "exportar_help_desk?inicio="+fecha_inicio+"&fin="+fecha_fin;
    window.open(dir);    
}

function likeImage(id){
    var cont = $("#img-"+id).find("i").html();
    cont = parseInt(cont);
    cont = cont + 1;
                
    $.ajax({
        type: "GET",
        url: "../likeImage",
        data: { id: id, cont:cont,}
    })
    .done(function( data ) {
        if(data=="1"){
            $("#img-"+id).find("i").removeClass("icon-thumbs-up-alt");
            $("#img-"+id).find("i").addClass("icon-thumbs-up");                    
            $("#img-"+id).find("i").html(" "+cont);
            $("#img-"+id).addClass("disabled");
                
        }else{
            toastr.error('No ha sido posible guardar su Like', 'Error');             
        }              
    });            
}



$('#name_new_profile').keyup(function(){
    var max = 8;

    if($("#name_new_profile").val().length>max){
        var name_new_profile = $("#name_new_profile").val();

        var exist = $.ajax({
            type:"GET",
            url:"exist_new_profile",
            data:"name_new_profile="+name_new_profile,
            async:false


        }).responseText;

        if (exist == 2) {
            $("#name_new_profile").css("border-color","#468847");
            $("#create_new_profile").removeClass("disabled");
        }else{
            $("#name_new_profile").css("border-color","#b94a48");
            $("#create_new_profile").addClass("disabled");
        }
    }
});


$('#edit_processes').change(function(event){   
    var process = $("#edit_processes").find(':selected').val();
    
    $("#profiles_cahge").load('selected_profile_process?process='+process);
    
    //$("#profile_css").css("display", "block");
});

$('#edit_process').change(function(event){   
    var process = $("#edit_process").find(':selected').val();
    
    $("#profile").load('selected_profile_process?process='+process);
    
    //$("#profile_css").css("display", "block");
});

$('#profiles_cahge').change(function(event){
    var profile = $("#profiles_cahge").find(':selected').val();
    
   $("#permisos_user").load('selected_submenu_profile?profile='+profile);      
    
});

$('#add_processes').change(function(event){   
    var process = $("#add_processes").find(':selected').val();
    
    $("#profiles_change_add").load('selected_profile_process?process='+process);
        
});

$('#profiles_change_add').change(function(event){
    var profile = $("#profiles_change_add").find(':selected').val();
    
   $("#permisos_user_add").load('selected_submenu_profile?profile_add='+profile);      
    
});

window.addEventListener('focus', function() {
    document.title = 'Indoamericana';
});
           
 //ingresamos en base de datos el login del usuario
   $.ajax({
        type: "GET",
        url: "actIngresos",
        data: { cont:"1"}
        })
        .done(function( data ) {
        //alert(data);
    });
    
//funcion para guardar una nueva FAQ
function guardarfaq(){
    var categoria = $("#categoria").find(':selected').val();
    var pregunta = $("#pregunta").val();
    var descripcion = CKEDITOR.instances['descripcion'].getData();
    
    var html = $.ajax({
    type: "GET",
    url: "guardarfaq",
    data: { categoria: categoria, pregunta: pregunta, descripcion: descripcion},
    async: false
    }).responseText;
       
    if (parseInt(html) == 1){
        document.getElementById("form").reset();
        CKEDITOR.instances['descripcion'].setData("");
        toastr.success('La nueva FAQ se ha guardado correctamente', 'Nueva FAQ');

    }else{
        toastr.error('No ha sido posible guardar la nueva FAQ', 'Error');
    }
}
