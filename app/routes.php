<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

//rutas para proceso de logueo en el sistema

Route::get('/', function() {
    if (Auth::user()) {
        return Redirect::to('informacion');
    } else {
        return View::make('login');
    }
});
Route::post('login', 'UserLogin@user');

Route::get('login2', array('before' => 'auth.basic', function() {
        return View::make('hello');
    }));

Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('/');
});

Route::get('comprobar', 'UserLogin@comprobar');
Route::post('recuperar', 'UserLogin@recuperar');

//routes of administration
Route::get('usuarios', 'SistemaController@usuarios');
Route::get('admin', array('uses' => 'MenusController@traerMenus'));
Route::get('guardarPerfil', 'UsersController@guardarPerfil');
Route::get('eliminarPermisos', 'UsersController@eliminarPermisos');
Route::get('agregarPermisos', 'UsersController@agregarPermisos');

Route::controller('users', 'UsersController');

//routes of help-desk 
Route::get('my_supports', 'HelpDeskController@my_supports');
Route::post('newSupport', 'HelpDeskController@newSupport');
Route::get('support_faq', 'HelpDeskController@support_faq');
Route::get('my_supports_pending', 'HelpDeskController@my_supports_pending');
Route::get('supportsPending', 'HelpDeskController@allmySupports');
Route::get('cancel_support', 'HelpDeskController@cancel_support');
Route::get('my_supports_history', 'HelpDeskController@my_supports_history');
Route::post('rating_support', 'HelpDeskController@rating_support');
Route::get('submit_rating_support', 'HelpDeskController@submit_rating_support');
Route::get('my_supports_faq', 'HelpDeskController@my_supports_faq');
Route::get('statistics', 'HelpDeskController@statistics');
Route::get('gestionfaq', 'HelpDeskController@gestionfaq');
Route::get('guardarfaq', 'HelpDeskController@guardarfaq');

Route::get('admin_supports', 'HelpDeskController@admin_supports');
Route::post('gestionajax', 'HelpDeskController@supportajax');
Route::get('asing_support', 'HelpDeskController@asing_support');
Route::post('newSupportAsign', 'HelpDeskController@newSupportAsign');
Route::get('adminHistorico', 'HelpDeskController@admin_history');
Route::get('admin_history', 'HelpDeskController@admin_history');
Route::get('my_supports_asign_history', 'HelpDeskController@my_supports_asign_history');
Route::get('statistics_tec', 'HelpDeskController@statistics_tec');
Route::get('respond_supports', 'HelpDeskController@respond_supports');


Route::get('my_support_asing', 'HelpDeskController@my_support_asing');
Route::get('attend_support', 'HelpDeskController@attend_support');
Route::post('atendSupport', 'HelpDeskController@atendSupport');

Route::get('allSupports', 'HelpDeskController@allSupports');
Route::controller('edit', 'HelpDeskController');

Route::get('notnewsupport', 'HelpDeskController@notnewsupport');
Route::get('notCloseSupport', 'HelpDeskController@notCloseSupport');
Route::get('notScaledSupport', 'HelpDeskController@notScaledSupport');
Route::get('notasignsupport', 'HelpDeskController@notasignsupport');

Route::get('exportar_help_desk', 'HelpDeskController@exportar_help_desk');
Route::get('graphic_statuses', 'HelpDeskController@graphic_statuses');
Route::get('graphic_user', 'HelpDeskController@graphic_user');
Route::get('graphic_category', 'HelpDeskController@graphic_category');
Route::get('graphic_technical', 'HelpDeskController@graphic_technical');
Route::get('graphic_rating', 'HelpDeskController@graphic_rating');
Route::get('graphic_time', 'HelpDeskController@graphic_time');
Route::get('graphic_technical', 'HelpDeskController@graphic_technical');
Route::get('graphic_prueba', 'HelpDeskController@graphic_prueba');
Route::get('technical_graphic', 'HelpDeskController@technical_graphic');
Route::get('technical_graphic_user', 'HelpDeskController@technical_graphic_user');


Route::get('categories', 'HelpDeskController@categories');
Route::get('desactive_notification', 'HelpDeskController@desactive_notification');
Route::get('prueba', 'HelpDeskController@prueba');
//end of routes of help-desk

//routes of Intranet 
Route::get('listaEmpleados', array('uses' => 'IntranetController@listaEmpleados'));
Route::get('informacion', array('uses' => 'IntranetController@informacionGeneral'));
Route::get('proceso/{id}', array('uses' => 'IntranetController@Proceso'));
Route::post('callUserProfile', 'IntranetController@callUserProfile');
Route::get('perfil', 'IntranetController@Perfil');
Route::get('actualizarperfil', 'IntranetController@actualizarperfil');
Route::get('cambiarpass', 'IntranetController@cambiarpass');
Route::get('redes', 'IntranetController@redes');
Route::get('funcionarios', 'IntranetController@funcionarios');
Route::get('resena', 'IntranetController@resena');
Route::get('cargardocumentos', 'IntranetController@cargarDocumentos');
Route::get('crearDir', 'IntranetController@crearDir');
Route::post('guardarArc', 'IntranetController@guardarArc');
Route::get('estadisticas', 'IntranetController@estadisticas');
Route::get('graphic_ingresos', 'IntranetController@graphic_ingresos');
Route::get('changedate', 'IntranetController@changedate');
Route::get('exportar', 'IntranetController@exportar');
Route::get('likeImage', 'IntranetController@likeImage');
Route::get('actIngresos', 'IntranetController@actIngresos');
Route::get('actTutorial', 'IntranetController@actTutorial');
Route::get('mensajes', 'IntranetController@mensajes');
Route::get('conversacion', 'IntranetController@conversacion');
Route::get('nuevomsj', 'IntranetController@nuevomsj');
Route::get('search_users', 'IntranetController@search_users');
Route::get('newmessage', 'IntranetController@newmessage');

//end

Route::controller('menus', 'MenusController');
Route::controller('pqrs', 'PqrsController');

/* App::missing(function($exception)
  {

  return Response::view('errors.page404', array(), 404);

  }); */

Route::group(array('prefix' => 'api'), function() {
    Route::resource('comments', 'UsersController@create');
});


Route::get('my_pqrs', 'PqrsController@my_pqrs');
Route::get('responder', 'PqrsController@responder');
Route::get('historial', 'PqrsController@historial');
Route::get('gestion', 'PqrsController@gestion');
Route::controller('user/getuser', 'getuserController');
Route::post('historial', 'PqrsController@historialAjax');
Route::post('gestion', 'PqrsController@gestionAjax');
Route::post('responder', 'PqrsController@responderAjax');
Route::post('respMasivo', 'PqrsController@respMasivo');

Route::get('todos', function() {
    return Users::orderBy('id', 'dsc')->get();
});

Route::get('cityes', function() {
    return Cities::all();
});

// Route::post("todos", function() {
//     //var_dump(Input::all());
//     return Users::create(Input::all());
// });
Route::post("todos", "UsersController@create");

/* Route::filter('auth', function(){
  if (Auth::guest()) return Redirect::to('login')->with('mensaje', 'Debes iniciar sesion para ver esta pagina');
  });

  Route::filter('auth', function(){
  if(Auth::guest()) return Redirect::to('login');
  }); */



/* Route::get('admin', array('before' => 'auth', function()
  {
  //return View::make('dashboard.index')->with('content', 'container');
  Route::controller('users', 'UsersController');
  //return View::make('dashboard.index')
  //->with('container', 'dashboard.container')
  //->with('menu_activo', 'none');

  })); */


Route::get('edit_user'					       , 'UsersController@edit_user');
Route::get('edit_user2'                , 'UsersController@edit_user2');
Route::get('exist_new_profile'			   , 'UsersController@exist_new_profile');
Route::get('selected_profile_process'	 , 'UsersController@selected_profile_process');
Route::get('selected_submenu_profile'	 , 'UsersController@selected_submenu_profile');
Route::get('all_my_notifications'      , 'UsersController@all_my_notifications');
