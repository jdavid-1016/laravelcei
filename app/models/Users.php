<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Users extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $fillable = array('name', 'name2' ,'last_name' ,'last_name2','password','email_institutional','email','cell_phone','phone','user','type_document','document','city_id');
	//public static $table = 'users';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
        
    public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
        
        //relacion uno a muchos entre users y Administratives
	public function administratives(){
		return $this->hasMany('administratives');
	}

	//relacion uno a muchos entre users y requests
	public function pqrs(){
		return $this->hasMany('pqrs');
	}
        
        //relacion uno a muchos entre users y income_statistics
	public function IncomeStatistics(){
		return $this->hasMany('IncomeStatistics');
	}

	public function processes(){
		return $this->hasMany('processes');
	}

	//relacion uno a muchos entre users y traceabilitys
	public function traceabilitys(){
		return $this->hasMany('traceabilitys');
	}
        public function Supports(){
		return $this->hasMany('Supports');
	}
	public function SupportTraces(){
		return $this->hasMany('SupportTraces');
	}

	//un usuario pertenece a un proceso
	public function cities(){
		return $this->belongsTo('Cities');
	}

	public function DefaultText(){
		return $this->hasMany('DefaultText');
	}
	public function DefaultConfidential(){
		return $this->hasMany('DefaultConfidential');
	}


	//relación muchos a muchos entre usuarios y perfiles
	public function profiles(){
		return $this->belongsToMany('Profiles');
	}
	public function delete(){
		//eliminamos los request
		if (count($this->request)>0) {
			foreach ($$this->request as $request) {
				$request->delete();
			}
		}
		//eliminamos la información de la tabla menu_user con detach
		//que hace referencia al usuario
		if(count($this->menus)>0){
			$this->menus()->detach();
		}

		//eliminamos al usuario
		return parent::delete();  
	}

}
