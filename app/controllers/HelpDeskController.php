<?php

/**
 * 
 */
class HelpDeskController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }

    //funcion para mostrar la vista de agregar nueo soporte
    public function my_supports() {
        //$noti =count(Supports::where('support_status_id', 1)->get());


        $user = Users::find(Auth::user()->id);


        $supports = Supports::where('supports.support_status_id', '<>', '1')
                ->where('supports.support_status_id', '<>', '4')
                ->where('supports.support_status_id', '<>', '5')
                ->where('supports.support_status_id', '<>', '6')
                ->where('users_id', '=', Auth::user()->id)
                ->where('rating_status', '=', 0)
                ->get();

        $supports2 = Supports::where('support_status_id', '<>', '2')
                ->where('support_status_id', '<>', '3')
                ->where('support_status_id', '<>', '6')
                ->where('users_id', '<>', '127')
                ->where('users_id', '<>', '125')
                ->where('users_id', '<>', '131')
                ->where('users_id', Auth::user()->id)
                ->get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('pending', $supports)
                            ->with('old_support', $supports2)
                            ->with('container', 'dashboard.helpDesk.my_supports')
                            ->with('user', $user)
                            //->with('noti', $noti)
                            ->with('submenu_activo', 'Mis solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
    }

    //funcion para agregar un nuevo soporte
    public function newSupport() {
        $my_id = Auth::user()->id;
        //var_dump(Input::all());
        $support = new Supports;

        $support->message = Input::get('description');
        $support->user_priority = Input::get('priority');
        $support->priority = Input::get('priority');
        $support->support_category_id = Input::get('category');
        $support->users_id = $my_id;

        $support->save();

        $user = Users::all();
        $submenu_required = 4;
        $bandera = 0;
        foreach ($user as $key) {
            foreach ($key->Profiles as $perfil) {
                foreach ($perfil->submenus as $submenu) {
                    if ($submenu_required == $submenu->id) {
                        //echo $key->name." ".$key->last_name."<br>";
                        $notification = new Notifications;

                        $notification->description = 'Creo un nuevo soporte';
                        $notification->estate = '1';
                        $notification->type_notification_id = '1';
                        $notification->user_id = $key->id;
                        $notification->user_id1 = $my_id;
                        $notification->link = 'admin_supports';

                        $notification->save();
                    }
                }
            }
        }






        return Auth::user();
    }

    //funcion para mostrar sugerencias cuando el usuario crea un soporte
    public function support_faq() {

        // $support_message1 = "Lorem ipsum dolor sit prueba, Internet!!!";
        //        $support_category_id = "1";

        $support_message1 = Input::get('description');
        $support_category_id = Input::get('category');


        $caracteres_no_permitidos = array(",", ".", "!");


        $support_message = str_replace($caracteres_no_permitidos, "", $support_message1);

        $palabras_clave = explode(" ", $support_message);
        $arreglo = array();
        $j = 0;

        for ($i = 0; $i < count($palabras_clave); $i++) {

            if (strlen($palabras_clave[$i]) > 3) {

                $arreglo[$j] = $palabras_clave[$i];
                $j++;
            } else {
                echo "";
            }
        }

        $sql = "";
        $h = 0;
        foreach ($arreglo as $key) {
            if ($h == 0) {
                $sql .= "title LIKE '%" . $key . "%'";
            } else {
                $sql .= " or title LIKE '%" . $key . "%'";
            }
            $h++;
        }

        $sql2 = "SELECT * FROM support_faqs WHERE (" . $sql . ")";

        $cons_grafico = DB::select($sql2);

        if (count($cons_grafico) > 0) {
            return View::make('prueba')
                            ->with('faqs', $cons_grafico);
        }
        //"SELECT * FROM support_statuses WHERE"
    }

    //funcion para ver mis soportes pendientes
    public function my_supports_pending() {

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.helpDesk.my_supports_pending')
                        ->with('submenu_activo', 'Mis solicitudes')
                        ->with('menu_activo', 'Help Desk');
    }

    //funcion para cancelar soportes por parte del usuario logueado
    public function cancel_support() {
        $id = $_GET['support_id'];
        $my_id = Auth::user()->id;


        $id = e(Input::get('support_id'));
        $user_id = e($my_id);
        $observation = e('Soporte Anulado por el usuario');





        DB::update('update support_traces set observation = ?, user_id = ? where supports_id = ?', array($observation, $user_id, $id));


        $support = Supports::find($id);

        $support->support_status_id = "6";
        $support->rating_status = "1";

        if ($support->save()) {
            return Auth::user();
        } else {
            echo "2";
        }
    }

    //funcion para ver el historico de mis soportes
    public function my_supports_history() {
        $my_id = Auth::user()->id;
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();

        $technicals = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('id', 'LIKE', '%' . $codigo . '%')
                ->where('user_id', 'LIKE', '%' . $responsible . '%')
                ->where('user_priority', 'LIKE', '%' . $priority . '%')
                ->where('support_status_id', 'LIKE', '%' . $status . '%')
                ->where('supports.support_status_id', '<>', '1')
                ->where('supports.support_status_id', '<>', '4')
                ->where('supports.support_status_id', '<>', '5')
                ->where('supports.users_id', '=', $my_id)
                ->orderBy('id', 'desc')
                ->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);
            $fecha_close = date($supports[$i]->Supports->updated_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["closed_at"] = $fecha_close;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]['time'] = $this->interval_date($fecha, $fecha_close);

            $data[$i]['rating_status'] = $supports[$i]->rating_status;

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }
            if ($supports[$i]->Supports->support_status_id == 2) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['id_plus'] = '';
                    $data[$i]['scaled'] = 'icon-long-arrow-up';
                } else {

                    if ($supports[$i]->consecutive == 1) {
                        $data[$i]['scaled'] = 'icon-plus-sign';
                        $data[$i]['id_plus'] = $data[$i]["hid"];
                    } else {
                        $data[$i]['display'] = 'display:nne';
                        $data[$i]['scaled'] = '';
                    }

                    $data[$i]['display'] = '';
                    $data[$i]['ocult'] = '';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = 'hola2';
                $data[$i]['ocult'] = '';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 6)
                $data[$i]['label'] = 'label-warning';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.helpDesk.my_supports_histroy')
                        ->with('user', $user)
                        ->with('supports', $data)
                        ->with('applicants', $applicants)
                        ->with('technicals', $technicals)
                        ->with('submenu_activo', 'Mis solicitudes')
                        ->with('menu_activo', 'Help Desk');
    }

    //funcion para traer todos los soportes pendientes del usuario logueado
    public function allmySupports() {
        $my_id = Auth::user()->id;
        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '2')
                ->where('supports.support_status_id', '<>', '3')
                ->where('supports.support_status_id', '<>', '6')
                ->where('supports.users_id', '=', $my_id)
                ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->support_status_id == 5 or $supports[$i]->Supports->support_status_id == 4) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['scaled'] = 'icon-long-arrow-up';
                    $data[$i]['id_plus'] = '';
                } else {
                    if ($supports[$i]->consecutive == 1) {
                        $data[$i]['scaled'] = 'icon-plus-sign';
                    } else {
                        $data[$i]['scaled'] = '';
                    }
                    $data[$i]['display'] = '';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                    $data[$i]['ocult'] = '';
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = '';
                $data[$i]['ocult'] = $supports[$i]->Supports->id;
                ;
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';



            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }
        return $data;
    }

    //funcion para traer el modal para calificar el soporte por parte del usuario
    public function rating_support() {
        $id = $_POST['valorCaja1'];

        $data = Supports::find($id);
        $questions = DB::table('support_questions')->get();

        return View::make('dashboard.helpDesk.rating_support')
                        ->with('questions', $questions)
                        ->with('data', $data);
    }

    //funcion para enviar la calificacion de un soporte por parte del usuario logueado
    public function submit_rating_support() {


        $support_id = Input::get('support_id');

        $support_rating_1 = New SupportRatings;

        $support_rating_1->support_id = Input::get('support_id');
        $support_rating_1->support_question_id = 1;
        $support_rating_1->value = Input::get('question_1');

        $support_rating_2 = New SupportRatings;

        $support_rating_2->support_id = Input::get('support_id');
        $support_rating_2->support_question_id = 2;
        $support_rating_2->value = Input::get('question_2');

        $support_rating_3 = New SupportRatings;

        $support_rating_3->support_id = Input::get('support_id');
        $support_rating_3->support_question_id = 3;
        $support_rating_3->value = Input::get('question_3');



        $observation = e(Input::get('comment'));

        $id = e(Input::get('support_id'));

        DB::update('update supports set rating_status = 1, comment = ?  where id = ?', array($observation, $id));


        if ($support_rating_1->save() && $support_rating_2->save() && $support_rating_3->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    //funcion para ver mis solicitudes faq
    public function my_supports_faq() {
        $user = Users::find(Auth::user()->id);
        $categories = SupportCategory::where('status', 1)
                ->get();


        $support_faqs = SupportFaqs::orderBy('support_category_id', 'asc')
                ->get();


        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.helpDesk.my_supports_faq')
                            ->with('user', $user)
                            ->with('categories', $categories)
                            ->with('support_faqs', $support_faqs)
                            ->with('submenu_activo', 'Mis solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
    }

    //funcion para administrar soportes pendientes
    public function admin_supports() {

        $permission = $this->permission_control("4");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }

        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '2')
                ->where('supports.support_status_id', '<>', '3')
                ->where('supports.support_status_id', '<>', '6')
                ->orderBy('support_status_id', 'asc')
                ->orderBy('id', 'desc')
                ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]["update_at"] = $supports[$i]->observation;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['hours'] = $this->calcula_time($fecha, $fecha_actual);





            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->support_status_id == 5 or $supports[$i]->Supports->support_status_id == 4) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['scaled'] = 'icon-long-arrow-up';
                    $data[$i]['id_plus'] = '';
                } else {
                    if ($supports[$i]->consecutive == 1) {
                        $data[$i]['scaled'] = 'icon-plus-sign';
                    } else {
                        $data[$i]['scaled'] = '';
                    }
                    $data[$i]['display'] = '';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                    $data[$i]['ocult'] = '';
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = '';
                $data[$i]['ocult'] = '';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-primary';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';



            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
                $data[$i]['row_color'] = 'yellow';

                if ($supports[$i]->Supports->support_status_id == 4) {
                    $data[$i]['row_color'] = 'green';
                }
            }

            if ($data[$i]['hours'] > 7200)
                $data[$i]['row_color'] = 'red';
        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_admin_supports')
                            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
                            ->with('supports', $data)
                            ->with('container', 'dashboard.helpDesk.adminHelp_desk')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
    }

    //funcion para ver el historico de los soportes por parte del administrador
    public function admin_history() {
        $permission = $this->permission_control("4");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();
        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        $codigo = Input::get('code');
        $applicant = Input::get('applicant');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('id', 'LIKE', '%' . $codigo . '%')
                ->where('users_id', $var, $signo)
                ->where('user_id', 'LIKE', '%' . $responsible . '%')
                ->where('user_priority', 'LIKE', '%' . $priority . '%')
                ->where('support_status_id', 'LIKE', '%' . $status . '%')
                ->where('supports.support_status_id', '<>', '1')
                ->where('supports.support_status_id', '<>', '4')
                ->where('supports.support_status_id', '<>', '5')
                ->orderBy('id', 'desc')
                ->paginate(15);

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);
            $fecha_close = date($supports[$i]->Supports->updated_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["closed_at"] = $fecha_close;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]['time'] = $this->interval_date($fecha, $fecha_close);
            $data[$i]['rating_status'] = $supports[$i]->rating_status;
            $data[$i]['comment'] = $supports[$i]->comment;

            $data[$i]['rating'] = round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'));
            $data[$i]['rating'] = (round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'), 2) > 0) ? round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'), 2) : 'pendiente';


            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }
            if ($supports[$i]->Supports->support_status_id == 2) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['id_plus'] = '';
                    $data[$i]['scaled'] = 'icon-long-arrow-up';
                } else {

                    if ($supports[$i]->consecutive == 1) {
                        $data[$i]['scaled'] = 'icon-plus-sign';
                        $data[$i]['id_plus'] = $data[$i]["hid"];
                    } else {
                        $data[$i]['display'] = 'display:nne';
                        $data[$i]['scaled'] = '';
                    }

                    $data[$i]['display'] = '';
                    $data[$i]['ocult'] = '';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = 'hola2';
                $data[$i]['ocult'] = '';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 6)
                $data[$i]['label'] = 'label-warning';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.helpDesk.admin_historico')
                        ->with('supports', $data)
                        ->with('pag', $supports)
                        ->with('applicants', $applicants)
                        ->with('technicals', $technical)
                        ->with('submenu_activo', 'Administrar Solicitudes')
                        ->with('menu_activo', 'Help Desk');
    }

    //funcion para mostrar el modal para asignar los soportes a tos tecnicos
    public function supportajax() {
        $id = $_POST['valorCaja1'];

        $data = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports_id', '=', $id)
                ->where('scale', '=', '0')
                ->get();

        //$data = Supports::find($id);
        $status = SupportStatus::all();
        $categories = SupportCategory::where('status', 1)->get();

        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        return View::make('dashboard.helpDesk.admin_supports_edit')
                        ->with('statuses', $status)
                        ->with('categories', $categories)
                        ->with('technicals', $technical)
                        ->with('data', $data);
    }

    //funcion para asignar el soporte a un tecnico
    public function asing_support() {


        $my_id = Auth::user()->id;
        $support_id = Input::get('support_id');

        $user_asign = Input::get('responsible');
        $observation = Input::get('observation');
        $state = Input::get('status');

        $support = Supports::find($support_id);

        if ($user_asign == "") {
            if ($state != 3) {
                echo "2";
                exit;
            } else {
                if ($observation == "") {
                    echo "2";
                    exit;
                }
                $notification = new Notifications;

                $notification->description = 'Respondio su soporte.';
                $notification->estate = '1';
                $notification->type_notification_id = '1';
                $notification->user_id = $support->users_id;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = "my_supports_history";

                $notification->save();

                $user_id = e($my_id);
            }
        } else {
            $user_id = e(Input::get('responsible'));
        }



        $support->support_status_id = Input::get('status');
        $support->user_priority = Input::get('priority');
        $support->support_category_id = Input::get('category');
        $observation = e(Input::get('observation'));

        $id = e(Input::get('support_id'));
        $consecutive = e('0');




        DB::update('update support_traces set observation = ?,consecutive = ?, user_id = ? where supports_id = ?', array($observation, $consecutive, $user_id, $id));

        if ($state == 3) {
            
        } else {
            $notification = new Notifications;

            $notification->description = 'Le asigno un nuevo soporte';
            $notification->estate = '1';
            $notification->type_notification_id = '1';
            $notification->user_id = $user_id;
            $notification->user_id1 = $my_id;
            $notification->link = "respond_supports";

            $notification->save();
        }




        if ($support->save()) {
            return Auth::user();
        } else {
            echo "2";
        }
    }

    //funcion para traer todos lo soportes que tene el tecnico asignado
    public function my_support_asing() {

        $permission = $this->permission_control("5");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
        $my_id = Auth::user()->id;
        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '2')
                ->where('supports.support_status_id', '<>', '3')
                ->where('user_id', '=', $my_id)
                ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-warning';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }
        return $data;
    }

    //funcion para traer el modal para atender el soporte por parte del tecnico
    public function atendSupport() {
        $id = $_POST['valorCaja1'];

        $data = Supports::find($id);
        $status = SupportStatus::all();
        $categories = SupportCategory::all();

        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        return View::make('dashboard.helpDesk.attend_support')
                        ->with('statuses', $status)
                        ->with('categories', $categories)
                        ->with('technicals', $technical)
                        ->with('data', $data);
    }

    //funcion para responder soportes por parte del tecnico
    public function respond_supports() {
        $my_id = Auth::user()->id;
        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '2')
                ->where('supports.support_status_id', '<>', '3')
                ->where('supports.support_status_id', '<>', '6')
                ->where('consecutive', '<>', '1')
                ->where('user_id', '=', $my_id)
                ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';
            $data[$i]['button_style'] = "";
            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5) {
                $data[$i]['label'] = 'label-success';
                if ($supports[$i]->scale == 0) {
                    $data[$i]['button_style'] = 'disabled';
                }
                if ($supports[$i]->consecutive == 1) {
                    $data[$i]['button_style'] = 'disabled';
                }
            }



            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }




        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_respond_supports')
                            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.helpDesk.respond_supports')
                            ->with('submenu_activo', 'Atender Solicitudes')
                            ->with('supports', $data)
                            ->with('menu_activo', 'Help Desk');
        }
    }

    //funcion para atender un soporte por parte del tecnico
    public function attend_support() {



        if (Input::get('user_asign') == "") {

            $support_id = Input::get('support_id');
            $support = Supports::find($support_id);


            if (Input::get('status') == 2) {

                $notification = new Notifications;

                $notification->description = 'Respondio su soporte.';
                $notification->estate = '1';
                $notification->type_notification_id = '1';
                $notification->user_id = $support->users_id;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = "my_supports_history";

                $notification->save();
            }

            $support->support_status_id = Input::get('status');
            $observation = e(Input::get('observation'));
            $id = e(Input::get('support_id'));

            DB::update('update support_traces set observation = ? where supports_id = ?', array($observation, $id));


            if ($support->save()) {
                return Auth::user();
            } else {
                echo "2";
            }
        } else {
            $support_id = Input::get('support_id');

            $support = Supports::find($support_id);

            $support->support_status_id = Input::get('status');

            $observation = Input::get('observation');
            $id = Input::get('support_id');
            $user_asign = Input::get('user_asign');

            $observation2 = e(Input::get('observation'));
            $ids = e(Input::get('support_id'));
            $consecutive = e('1');

            DB::update('update support_traces set observation = ?, consecutive = ? where supports_id = ?', array($observation2, $consecutive, $ids));
            $traces = new SupportTraces;

            $traces->supports_id = $support_id;
            $traces->observation = $observation;
            $traces->scale = "1";
            $traces->consecutive = "2";
            $traces->user_id = $user_asign;

            $traces->save();



            if ($support->save()) {

                $notification = new Notifications;

                $notification->description = 'Te escalo un soporte.';
                $notification->estate = '1';
                $notification->type_notification_id = '1';
                $notification->user_id = $user_asign;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = "respond_supports";
                $notification->save();

                return Auth::user();
            } else {
                echo "2";
            }
        }
    }

    //funcion para ver el historico de los soportes  asignados al tecnico
    public function my_supports_asign_history() {
        $permission = $this->permission_control("5");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
        $my_id = Auth::user()->id;
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();
        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $text = Input::get('text');

        if (isset($_GET['text']) && !empty($_GET['text'])) {
            $var_text = 'LIKE';
            $signo_text = "%" . $text . "%";
        } else {
            $var_text = 'LIKE';
            $signo_text = "%" . $text . "%";
        }

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('id', 'LIKE', '%' . $codigo . '%')
                ->where('users_id', $var, $signo)
                ->where('observation', $var_text, $signo_text)
                ->where('user_id', 'LIKE', '%' . $responsible . '%')
                ->where('user_priority', 'LIKE', '%' . $priority . '%')
                ->where('support_status_id', 'LIKE', '%' . $status . '%')
                ->where('supports.support_status_id', '<>', '1')
                ->where('supports.support_status_id', '<>', '4')
                ->where('supports.support_status_id', '<>', '5')
                ->where('user_id', $my_id)
                ->orderBy('id', 'desc')
                ->paginate(15);

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);
            $fecha_close = date($supports[$i]->Supports->updated_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["closed_at"] = $fecha_close;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]['time'] = $this->interval_date($fecha, $fecha_close);
            $data[$i]['hours'] = $this->calcula_time($fecha, $fecha_close);
            $data[$i]['rating_status'] = $supports[$i]->rating_status;
            $data[$i]['comment'] = $supports[$i]->comment;

            $data[$i]['rating'] = round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'));
            $data[$i]['rating'] = (round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'), 2) > 0) ? round(DB::table('support_ratings')->where('support_id', $data[$i]['hid'])->avg('value'), 2) : 'pendiente';


            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 6)
                $data[$i]['label'] = 'label-warning';




            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';
        }

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.helpDesk.attend_support_history')
                        ->with('supports', $data)
                        ->with('pag', $supports)
                        ->with('applicants', $applicants)
                        ->with('technicals', $technical)
                        ->with('submenu_activo', 'Atender Solicitudes')
                        ->with('menu_activo', 'Help Desk');
    }

    //funcion para notificacion de nuevo soporte
    public function notnewsupport() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 4;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }

    //funcion para notificacion de asignar soporte
    public function notasignsupport() {
        if (Input::get('responsible') == Auth::user()->id) {
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            echo $notificaciones->last()->id;
        } else {
            echo "0";
        }
    }

    //funcion para notificacion de soportre cerrado
    public function notCloseSupport() {
        $my_id = Auth::user()->id;
        $id_support = Input::get('id_support');
        $support = Supports::find($id_support);



        if ($support->users_id == Auth::user()->id) {
            //$notification->save();
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            echo $notificaciones->last()->id;
        } else {
            echo "2";
        }
    }

    //funcion para notificacion de Escalar soporte
    public function notScaledSupport() {

        $my_id = Auth::user()->id;
        $id_support = Input::get('id_support');
        $support = Supports::find($id_support);
        $user_asign = Input::get('user_asign');

        //$notification->save();
        if ($my_id == $user_asign) {

            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            echo $notificaciones->last()->id;
            //echo $my_id;
        } else {
            echo "0";
        }
    }

    //fruncion para calcular el tiempo transcurrido de los soportes
    public function interval_date($init, $finish) {
        //formateamos las fechas a segundos tipo 1374998435
        $diferencia = strtotime($finish) - strtotime($init);

        //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
        //floor devuelve el número entero anterior, si es 5.7 devuelve 5
        if ($diferencia < 60) {
            $tiempo = floor($diferencia) . " Seg";
        } else if ($diferencia > 60 && $diferencia < 3600) {
            $tiempo = floor($diferencia / 60) . ' Min';
        } else if ($diferencia > 3600 && $diferencia < 86400) {
            $tiempo = floor($diferencia / 3600) . " Horas";
        } else if ($diferencia > 86400 && $diferencia < 2592000) {
            $tiempo = floor($diferencia / 86400) . " Días";
        } else if ($diferencia > 2592000 && $diferencia < 31104000) {
            $tiempo = floor($diferencia / 2592000) . " Meses";
        } else if ($diferencia > 31104000) {
            $tiempo = floor($diferencia / 31104000) . " Años";
        } else {
            $tiempo = "Error";
        }
        return $tiempo;
    }

    function calcula_time($init, $finish) {
        $diferencia = strtotime($finish) - strtotime($init);

        return $diferencia;
    }

    //funcion para desactivar las notificaciones del helpDesk
    public function desactive_notification() {

        $id_not = Input::get('valorCaja1');

        $notification = Notifications::find($id_not);

        $notification->estate = '0';

        if ($notification->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    //funcion para consultar las categorias de sopportes
    public function categories() {
        return SupportCategory::orderBy('name', 'asc')->get();
    }

    //funciones para hacer las graficas del sitema helpDesk

    public function graphic_technical() {


        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);


        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM support_traces
				JOIN supports ON (supports.id = support_traces.supports_id) 
				JOIN users ON ( users.id = support_traces.user_id )";
        if (Input::get('fecha_fin')) {
            $sql.= "AND supports.updated_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.= " AND supports.support_status_id = 2
				AND support_traces.scale = 0
				GROUP BY user_id
				ORDER BY dato DESC 
				";

        $cons_grafico = DB::select($sql);


        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
          and supports.support_status_id = 2
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Tecnicos Detalle');


        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Tecnicos Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico[$i]->name . " " . $cons_grafico[$i]->last_name)
                    ->setCellValue('B' . $j, $cons_grafico[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Tecnicos_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;



        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes Atendidos por los Tecnicos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function graphic_statuses() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT support_status_id, support_statuses.name, COUNT( * )  as dato
				FROM supports
				join support_statuses on (support_statuses.id = supports.support_status_id)";
        if (Input::get('fecha_fin')) {
            $sql.= " where supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.="  GROUP BY support_status_id";
        $cons_grafico = DB::select($sql);



        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);



        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados Detalle');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Estados');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Estado')
                ->setCellValue('B1', 'Valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico[$i]->name)
                    ->setCellValue('B' . $j, $cons_grafico[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Estados_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;
        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }



        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Estados");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#FE2E64@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#30D9CB@0.5', '#30D9CB@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function graphic_user() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM supports
				JOIN users ON ( users.id = supports.users_id )
				where users.id <> 127
				and users.id <> 125 
				and users.id <> 131";

        if (Input::get('fecha_fin')) {
            $sql.= " AND supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.= " GROUP BY users_id
				ORDER BY dato DESC 
				LIMIT 0 , 7";
        $cons_grafico = DB::select($sql);

        $sql3 = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato FROM supports JOIN users ON ( users.id = supports.users_id ) where users.id <> 127 and users.id <> 125 and users.id <> 131 GROUP BY users_id ORDER BY dato DESC";

        $cons_grafico3 = DB::select($sql3);

        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Usuarios');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico3); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico3[$i]->name . " " . $cons_grafico3[$i]->last_name)
                    ->setCellValue('B' . $j, $cons_grafico3[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Usuarios_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }


        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Usuarios");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#FE2E64@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#F7819F@0.5', '#F7819F@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function graphic_rating() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users_id, users.name,users.last_name, avg(support_ratings.value) AS dato
				FROM support_traces
				JOIN supports ON (supports.id = support_traces.supports_id)
				JOIN support_ratings ON (support_ratings.support_id = supports.id)
				JOIN users ON ( users.id = support_traces.user_id ) 
				WHERE supports.support_status_id = 2";
        if (Input::get('fecha_fin')) {
            $sql.= " AND supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }

        $sql.= " GROUP BY user_id
				
				ORDER BY dato desc 
				";
        $cons_grafico = DB::select($sql);

        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
          AND rating_status = 1
          AND support_status_id <> 6 
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO')
                ->setCellValue('M1', 'PREGUNTA_1')
                ->setCellValue('N1', 'PREGUNTA_2')
                ->setCellValue('O1', 'PREGUNTA_3');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {


            $ratings = SupportRatings::where('support_id', $cons_grafico2[$i]->hid)->get();

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2)
                    ->setCellValue('M' . $j, $ratings[0]->value)
                    ->setCellValue('N' . $j, $ratings[1]->value)
                    ->setCellValue('O' . $j, $ratings[2]->value);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Puntaje_P');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico[$i]->name)
                    ->setCellValue('B' . $j, $cons_grafico[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Calificaciones_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }


        $grafico1 = new Graph(400, 400);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Calificaciones");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#6BAEE4@0.5', '#6BAEE4@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function graphic_time() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users.name, users.last_name, avg(TIMESTAMPDIFF(HOUR, supports.created_at, supports.closed_at)) as promedio
				from (supports)
				join support_traces on(support_traces.supports_id = supports.id)
				join users on (users.id = support_traces.user_id)
				where supports.support_status_id = 2";
        if (Input::get('fecha_fin')) {
            $sql.= " AND supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.= " group by users.name
				order by promedio DESC
				";
        $cons_grafico = DB::select($sql);

        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.updated_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
          AND support_status_id <> 1
          AND support_status_id <> 4
          AND support_status_id <> 5
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->updated_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Tiempo_Horas');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico[$i]->name . " " . $cons_grafico[$i]->last_name)
                    ->setCellValue('B' . $j, $cons_grafico[$i]->promedio);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Tiempos_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;
        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {

                $datos1[] = $key->promedio;
                $datos1_2[] = $key->name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }



        $grafico1 = new Graph(400, 400);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Tiempo de Respuesta");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#E6B54A@0.5', '#E6B54A@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function graphic_category() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT support_category_id, support_categories.name, COUNT( * ) AS dato
				FROM supports
				JOIN support_categories ON ( support_categories.id = supports.support_category_id )";
        if (Input::get('fecha_fin')) {
            $sql.= "WHERE supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }

        $sql.=" GROUP BY support_category_id
				ORDER BY dato DESC 
				LIMIT 0 , 7";
        $cons_grafico = DB::select($sql);

        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
		 supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
		  observation, name.name as tec_name, name.last_name as tec_name2 
		  from supports 
		  join support_statuses on (support_statuses.id = supports.support_status_id) 
		  join support_categories on (supports.support_category_id = support_categories.id) 
		  join users on (users.id = supports.users_id) 
		  join support_traces on (supports.id = support_traces.supports_id) 
		  join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
		  ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $sql3 = "SELECT support_category_id, support_categories.name, COUNT( * ) AS dato FROM supports JOIN support_categories ON ( support_categories.id = supports.support_category_id ) GROUP BY support_category_id ORDER BY dato DESC";

        $cons_grafico3 = DB::select($sql3);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Categoria')
                ->setCellValue('B1', 'Valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico3); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico3[$i]->name)
                    ->setCellValue('B' . $j, $cons_grafico3[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Categorias_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;
        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }



        $grafico1 = new Graph(400, 400);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Categoria");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#AC58FA@0.5', '#AC58FA@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function statistics() {
        $permission = $this->permission_control("2");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
        $user = Users::find(Auth::user()->id);




        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.helpDesk.statistics')
                            ->with('user', $user)
                            ->with('submenu_activo', 'Estadísticas')
                            ->with('menu_activo', 'Help Desk');
        }
    }

    public function statistics_tec() {
        $permission = $this->permission_control("2");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
        $user = Users::find(Auth::user()->id);




        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.helpDesk.statistics_tec')
                            ->with('user', $user)
                            ->with('submenu_activo', 'Atender Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }
    }

    public function graphic_prueba() {

        $permission = $this->permission_control("3");

        echo $permission;
        // $fecha = Input::get("fecha_fin");            
        // $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
        // $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        // $grafico1 = array();
        // include("jpgraph/src/jpgraph.php");
        // include("jpgraph/src/jpgraph_bar.php");
        // include("assets/phpexcel/PHPExcel.php");
        // $sql = "SELECT support_category_id, support_categories.name, COUNT( * ) AS dato
        // 		FROM supports
        // 		JOIN support_categories ON ( support_categories.id = supports.support_category_id )";
        // if (Input::get('fecha_fin')) {
        // 	$sql.= "WHERE supports.created_at BETWEEN '".Input::get("fecha_inicio")."' AND '".$nuevafecha."'";
        // }
        // $sql.=" GROUP BY support_category_id
        // 		ORDER BY dato DESC 
        // 		LIMIT 0 , 7";
        // $cons_grafico = DB::select($sql);
        //     $objPHPExcel = new PHPExcel();
        // 	$objPHPExcel->getProperties()->setCreator("Indoamericana")
        //         ->setLastModifiedBy("Indoamericana")
        //         ->setTitle("Ingresos por usuario")
        //         ->setSubject("Indoamericana")
        //         ->setDescription("Indoamericana")
        //         ->setKeywords("Indoamericana")
        //         ->setCategory("Indoamericana");
        // 	$objPHPExcel->setActiveSheetIndex(0)
        //         ->setCellValue('A1', 'Usuario')
        //         ->setCellValue('B1', 'datos');
        // 	$j=2;
        // 	for ($i = 0; $i < count($cons_grafico); $i++) {
        //         $objPHPExcel->setActiveSheetIndex(0)
        //         ->setCellValue('A' . $j, $cons_grafico[$i]->name)
        //         ->setCellValue('B' . $j, $cons_grafico[$i]->dato);
        //         $j++;
        // 	}
        // 	$objPHPExcel->getActiveSheet()->setTitle('Estados');
        // 	// Creamos una nueva hoja llamada “Detalle”
        // 	$myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios');
        // 	$objPHPExcel->addSheet($myWorkSheet, 1);
        // 	$objPHPExcel->setActiveSheetIndex(1)
        //         ->setCellValue('A1', 'Usuario')
        //         ->setCellValue('B1', 'IP');
        // 	$j=2;
        // 	for ($i = 0; $i < count($cons_grafico); $i++) {
        //         $objPHPExcel->setActiveSheetIndex(1)
        //         ->setCellValue('A' . $j, $cons_grafico[$i]->name)
        //         ->setCellValue('B' . $j, $cons_grafico[$i]->dato);
        //         $j++;
        // 	}
        // 	$objPHPExcel->setActiveSheetIndex(0);
        //   	$date = date("d-m-Y");
        // 	// Redirect output to a client's web browser (Excel5)
        // 	if (isset($_GET['exportar'])) {
        // 		header('Content-Type: application/vnd.ms-excel');
        // 		header('Content-Disposition: attachment;filename="Ingresos_' . $date . '.xls"');
        // 		header('Cache-Control: max-age=0');
        // 		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        // 		$objWriter->save('php://output');
        // 	}
        // $total1 = count($cons_grafico);
        // $grafico1 = $cons_grafico;
        // if($total1>0){
        // 	foreach ($cons_grafico as $key) {
        //  		$datos1[] = $key->dato;
        //  		$datos1_2[] = $key->name;
        // 	}
        // }else{
        //     $datos1[] = 0;
        //     $datos1_2[] = "No hay datos";
        // 	}
        // $grafico1  = new Graph(400, 400);
        // $grafico1->SetScale("textlin");
        // $grafico1->title->Set("Reporte de Soportes por Categoria");
        // $grafico1->xaxis->SetTickLabels($datos1_2);
        // $grafico1->xaxis->SetLabelAngle(50);
        // $barplot1 = new BarPlot($datos1);
        // $barplot1->SetColor("#F781BE@0.5");
        // //UN GRADIANTE HORIZONTAL ROJO A AZUL
        // $barplot1->SetFillGradient('#AC58FA@0.5', '#AC58FA@0.5', GRAD_HOR);
        // //25 PIXELES DE ANCHO PARA CADA BARRA
        // $barplot1->SetWidth(25);
        // $grafico1->Add($barplot1);
        // $barplot1->value->SetFormat("%d");
        // $barplot1->value->Show();
        // $grafico1->Stroke();
    }

    //funcion para traer todos los soportes pendientes por parte del aministrador
    public function allSupports() {
        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '2')
                ->where('supports.support_status_id', '<>', '3')
                ->orderBy('support_status_id', 'asc')
                ->orderBy('id', 'desc')
                ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]["update_at"] = $supports[$i]->observation;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['hours'] = $this->calcula_time($fecha, $fecha_actual);





            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }

            if ($supports[$i]->Supports->support_status_id == 5 or $supports[$i]->Supports->support_status_id == 4) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['id_plus'] = '';
                    $data[$i]['scaled'] = '';
                } else {
                    $data[$i]['display'] = '';
                    $data[$i]['scaled'] = 'icon-plus-sign';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = '';
            }

            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-primary';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';



            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
                $data[$i]['row_color'] = 'yellow';

                if ($supports[$i]->Supports->support_status_id == 4) {
                    $data[$i]['row_color'] = 'green';
                }
            }

            if ($data[$i]['hours'] > 7200)
                $data[$i]['row_color'] = 'red';
        }
        return $data;
    }

    public function prueba() {

        // $menus = "";
        // $bandera = "";
        // $cont = 0;


        // $profile = "64";
        // //echo $profile;                
        // $data = Cities::all();
        // foreach ($data as $key) {
        //     echo "nombre = ".$key->Users->name."<br>";
        // }

        // return View::make('dashboard.users.nuevo_sidebar')
        // ->with('data', $data);

        $mensajes = Messages::all();
        foreach($mensajes as $msg){
            echo "msg ".$msg->message."<br>";
        }
    }

    public function gestionfaq() {

        $permission = $this->permission_control("22");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Gestion FAQ')
                            ->with('menu_activo', 'Help Desk');
        }
        $categorias = SupportCategory::orderBy('name', 'asc')->get();

        return View::make('dashboard.index')
                        ->with('content', 'container')
                        ->with('container', 'dashboard.helpDesk.gestionfaq')
                        ->with('categorias', $categorias)
                        ->with('menu_activo', 'Help Desk')
                        ->with('submenu_activo', 'Gestion FAQ');
    }

    public function guardarfaq() {
        
        $faq = new SupportFaqs;

        $faq->title = Input::get('pregunta');
        $faq->text = Input::get('descripcion');
        $faq->support_category_id = Input::get('categoria');
        
        if($faq->save()){
            echo "1";
        }else{
            echo "0";
        }
    }




    public function technical_graphic() {


        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);
        $my_id = Auth::user()->id;


        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                FROM support_traces
                JOIN supports ON (supports.id = support_traces.supports_id) 
                JOIN users ON ( users.id = support_traces.user_id )";
        if (Input::get('fecha_fin')) {
            $sql.= "AND supports.updated_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.= " AND supports.support_status_id = 2
                AND support_traces.scale = 0
                AND user_id = '$my_id' 
                GROUP BY user_id
                ORDER BY dato DESC 
                ";

        $cons_grafico = DB::select($sql);


        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
         supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
          observation, name.name as tec_name, name.last_name as tec_name2 
          from supports 
          join support_statuses on (support_statuses.id = supports.support_status_id) 
          join support_categories on (supports.support_category_id = support_categories.id) 
          join users on (users.id = supports.users_id) 
          join support_traces on (supports.id = support_traces.supports_id) 
          join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
          and supports.support_status_id = 2
          AND user_id = '$my_id' 
          ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Tecnicos Detalle');


        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Tecnicos Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico[$i]->name . " " . $cons_grafico[$i]->last_name)
                    ->setCellValue('B' . $j, $cons_grafico[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Tecnicos_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;



        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes Atendidos por los Tecnicos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }


    public function technical_graphic_user() {
        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);
        $my_id = Auth::user()->id;
        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                FROM supports
                JOIN users ON ( users.id = supports.users_id )
                JOIN support_traces ON ( support_traces.supports_id = supports.id )
                where user_id = '$my_id' 
                AND scale = 0";

        if (Input::get('fecha_fin')) {
            $sql.= " AND supports.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'";
        }
        $sql.= " GROUP BY users_id
                ORDER BY dato DESC 
                LIMIT 0 , 7";
        $cons_grafico = DB::select($sql);

        $sql3 = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato 
        FROM supports JOIN users ON ( users.id = supports.users_id ) 
        JOIN support_traces ON ( support_traces.supports_id = supports.id ) 
        where user_id = '$my_id' 
        GROUP BY users_id ORDER BY dato DESC";

        $cons_grafico3 = DB::select($sql3);

        $sql2 = "SELECT supports.id as hid, message, user_priority, rating_status, comment, supports.created_at as creacion,
         supports.closed_at, support_statuses.name as estado, support_categories.name as category, users.name as user_name, users.last_name as user_name2,
          observation, name.name as tec_name, name.last_name as tec_name2 
          from supports 
          join support_statuses on (support_statuses.id = supports.support_status_id) 
          join support_categories on (supports.support_category_id = support_categories.id) 
          join users on (users.id = supports.users_id) 
          join support_traces on (supports.id = support_traces.supports_id) 
          join users name on (support_traces.user_id = name.id)
          WHERE scale = 0
          AND user_id = '$my_id'
          ORDER BY `supports`.`id` DESC";

        $cons_grafico2 = DB::select($sql2);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'ID')
                ->setCellValue('B1', 'MENSAJE_USUARIO')
                ->setCellValue('C1', 'PRIORIDAD')
                ->setCellValue('D1', 'ESTADO_CALIFICACION')
                ->setCellValue('E1', 'COMENTARIO_USUARIO')
                ->setCellValue('F1', 'FECHA_CREACION')
                ->setCellValue('G1', 'FECHA_CERRADO')
                ->setCellValue('H1', 'ESTADO')
                ->setCellValue('I1', 'CATEGORIA')
                ->setCellValue('J1', 'USUARIO')
                ->setCellValue('K1', 'OBSERVACION_TECNICO')
                ->setCellValue('L1', 'TECNICO');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico2); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $cons_grafico2[$i]->hid)
                    ->setCellValue('B' . $j, $cons_grafico2[$i]->message)
                    ->setCellValue('C' . $j, $cons_grafico2[$i]->user_priority)
                    ->setCellValue('D' . $j, $cons_grafico2[$i]->rating_status)
                    ->setCellValue('E' . $j, $cons_grafico2[$i]->comment)
                    ->setCellValue('F' . $j, $cons_grafico2[$i]->creacion)
                    ->setCellValue('G' . $j, $cons_grafico2[$i]->closed_at)
                    ->setCellValue('H' . $j, $cons_grafico2[$i]->estado)
                    ->setCellValue('I' . $j, $cons_grafico2[$i]->category)
                    ->setCellValue('J' . $j, $cons_grafico2[$i]->user_name . " " . $cons_grafico2[$i]->user_name2)
                    ->setCellValue('K' . $j, $cons_grafico2[$i]->observation)
                    ->setCellValue('L' . $j, $cons_grafico2[$i]->tec_name . " " . $cons_grafico2[$i]->tec_name2);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Usuarios');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Usuarios Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'valor');

        $j = 2;
        for ($i = 0; $i < count($cons_grafico3); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $cons_grafico3[$i]->name . " " . $cons_grafico3[$i]->last_name)
                    ->setCellValue('B' . $j, $cons_grafico3[$i]->dato);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        if (isset($_GET['exportar'])) {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Usuarios_' . $date . '.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
        }



        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }


        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Soportes por Usuarios");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#FE2E64@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#F7819F@0.5', '#F7819F@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

}

?>