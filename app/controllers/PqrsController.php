<?php 	
/**
* 	
*/
class PqrsController extends BaseController
{
	public function __construct()
	{
		$this->beforeFilter('auth'); //bloqueo de acceso
	}

	public function getIndex()
	{
		$requests = Requests::where('state_requests_id', '==', 1 )->get();
		return View::make('dashboard.index')
		
		->with('requests',$requests)
		->with('container', 'dashboard.pqrs.gestion')
		->with('menu_activo', 'pqrs');		
		
		
	}
	public function my_pqrs()	{	
		$my_id = Auth::user()->id;

		$requests = Requests::where('user_id', '=',$my_id )->paginate(5);
		
		$TypeRequests 	= DB::table('Type_requests')->get();
		$process 		= DB::table('processes')->get();
		$TypeUsers 		= DB::table('type_users')->get();
		$detalle 		= DB::table('detalle')->get();

		//consulta trazabilidad
		$Traceabilitys = Traceabilitys::where('id', '<>', -2)->get();


		if (count($requests) == 0) {
			return View::make('dashboard.index')

			->with('error', 'Usted no ha ingresado nunguna PQR')

			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('menu_activo', 'pqrs');


			
		}else{
			return View::make('dashboard.index')
			->with('traceabilitys', $Traceabilitys)
			->with('requests', $requests)
			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('menu_activo', 'pqrs');
		}
	}

	public function gestion()
	{	
		$my_id = Auth::user()->id;
		$user = Users::find(Auth::user()->id);
		$pqrs = Pqrs::where('state_pqr_id', '=', '1')->orderBy('id','desc')->get();

		$Type_pqrs 				= DB::table('type_pqrs')->get();
		$confidential_texts 	= DB::table('confidential_texts')->where('administrative_id', '=', $my_id)->get();
		$specify 				= DB::table('specify')->get();
		$descriptions 			= DB::table('descriptions')->get();
		$states_pqrs 			= DB::table('states_pqrs')->get();
		$processes 				= processes::where('user_id', '=', 1)->get();
		$default_texts 			= DB::table('default_texts')->where('administrative_id', '=', $my_id)->get();
		

		return View::make('dashboard.index')
		->with('requests',$pqrs)
		->with('user', $user)
		->with('TypeRequests', $Type_pqrs)
		->with('especifique', $specify)
		->with('description', $descriptions)
		->with('state_requests', $states_pqrs)
		->with('processes', $processes)
		->with('default_text',$default_texts)
		->with('default_confidential',$confidential_texts)
		->with('container', 'dashboard.pqrs.gestion')
		->with('menu_activo', 'pqrs');

	}

	public function responder()	{
		
		$my_id = Auth::user()->id;

		$requests = Requests::where('state_requests_id', '=', '2')->where('user_asign', '=', $my_id)->orderBy('id','desc')->get();

		$TypeRequests 			= DB::table('Type_requests')->get();
		$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
		$especifique 			= DB::table('especifique')->get();
		$descripcion 			= DB::table('descripcion')->get();
		$state_requests 		= DB::table('state_requests')->get();
		$processes 				= User::where('responsible', '=', 1)->get();
		$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

		if (count($requests) == 0) {
			return View::make('dashboard.index')
			
			
			->with('container', 'dashboard.pqrs.gestion')
			->with('menu_activo', 'pqrs')
			->with('error', 'Usted no ha ingresado nunguna PQR');

		}else{
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('container', 'dashboard.pqrs.responder')
			->with('menu_activo', 'pqrs');

		}

		


	}

	public function historial(){
		$level = Auth::user()->rol_id;

		//control permissions only access administrator (ad)
		if($level==1)
		{
			$my_id = Auth::user()->id;
			$requests = Requests::where('state_requests_id', '<>', '0')->orderBy('id','desc')->paginate(10);
			$TypeRequests 			= DB::table('Type_requests')->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$descripcion 			= DB::table('descripcion')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

			//$requests = DB::table('requests')->paginate(6);
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');
			
			
		}else{
			return View::make('dashboard.index')
			->with('container', 'errors.access_denied_ad')
			->with('menu_activo', 'administration');
		}
	}

	public function getDelete($user_id)	{
		//buscamos el usuario en la base de datos segun la id enviada
		$user = User::find($user_id);
		//eliminamos y redirigimos
		$user->delete();
		return Redirect::to('users')->with('status', 'ok_delete');
	}

	public function postNew(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		$nombre_supports = $_FILES['support']['name'];
		$ruta = $_FILES['support']['tmp_name'];

		if (isset($_FILES['support']) && $_FILES['support']['tmp_name'] == "") {
			$destino = 'Sin siporte';
		}else{
			$destino = 'assets/supports_pqrs/'.'soporte_'.$_POST['user_id'].$nombre_supports;
			copy($ruta, $destino);
		}

		$request = new Requests;

		$request->title 			= 		Input::get('title');
		$request->description 		= 		Input::get('description');
		$request->type_requests_id 	= 		Input::get('type_requests_id');
		$request->date 				= 		Input::get('date');
		// $request->detalle 			=		Input::get('detalle');
		$request->user_id 			= 		Input::get('user_id');
		$request->process_id 		= 		Input::get('process_id');
		$request->type_users_id 	= 		Input::get('type_users_id');
		$request->support 			=		$destino;


		//guardamos
		$request->save();
		
		//redirigimos a my requets
		return Redirect::to('my_pqrs')->with('status', 'ok_create');
	}
	public function postText(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		

		$DefaultText = new DefaultText;

		$DefaultText->text 			= 		Input::get('text');
		$DefaultText->id_user 		= 		Input::get('user_id');
		$DefaultText->date 			= 		Input::get('date');
		//guardamos
		$DefaultText->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postConfidential(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		

		$DefaultText = new DefaultConfidential;

		$DefaultText->text 			= 		Input::get('text');
		$DefaultText->id_user 		= 		Input::get('user_id');
		$DefaultText->date 			= 		Input::get('date');
		//guardamos
		$DefaultText->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postReply(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('responder')->with('status', 'ok_create');
	}
	public function postResponse(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'type_request2' 	  =>Input::get('state_request'),
				'specify' 			  =>Input::get('specify'),
				'specify_description' =>Input::get('specify_description'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postMassive(){
		$data_request = array(
				'request_id' => Input::get('chck')
			);
		foreach ($data_request['request_id'] as $key) {
			echo $key;

			Requests::where('id', '=', $key)->update(
				array(
					'state_requests_id' 	=>Input::get('state_request'),
					'type_request2' 		=>Input::get('state_request'),
					'specify' 				=>Input::get('specify'),
					'specify_description' 	=>Input::get('specify_description'),
					'user_asign'		 	=>Input::get('user_asign')


				)
			);

			$traceability = new Traceabilitys;

			$traceability->coment 		= Input::get('coment');
			$traceability->date 		= Input::get('date');
			$traceability->users_coment = Input::get('users_coment');
			$traceability->confidential = Input::get('confidential');
			$traceability->user_id 		= Input::get('user_asign');
			$traceability->requests_id 	= $key;

			//guardamos
			$traceability->save();
		}


		return Redirect::to('gestion')->with('status', 'ok_create');
	}


	public function historialAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];

			$requests = Requests::where('id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}elseif (isset($_POST['valorCaja2']) && $_POST['valorCaja2'] != 'Proceso' && $_POST['valorCaja2'] != '' ) {

			$item = $_POST['valorCaja2'];

			$requests = Requests::where('process_id', '=', "$item")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja3']) && $_POST['valorCaja3'] != 'Estado' && $_POST['valorCaja3'] != '' ) {

			$item = $_POST['valorCaja3'];
		

			$requests = Requests::where('state_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja4']) && $_POST['valorCaja4'] != 'Tipo PQR' && $_POST['valorCaja4'] != '' ) {

			$item = $_POST['valorCaja4'];
			

			$requests = Requests::where('type_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}
		else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}

	}
	public function gestionAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];
			$my_id = Auth::user()->id;
			$requests = Requests::where('id', '=', $item)->paginate(10);
			foreach ($requests as $key) {
				$item_last = $key->user->id;
			}

			$previus_requests = Requests::where('user_id', '=', $item_last)->get();
			$TypeRequests 			= DB::table('Type_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$descripcion 			= DB::table('descripcion')->get();



			return View::make('containers.ajaxGestion')
			->with('requests',$requests)
			->with('previus_requests',$previus_requests)
			->with('TypeRequests',$TypeRequests)
			->with('processes',$processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('especifique',$especifique)
			->with('state_requests',$state_requests)
			->with('description',$descripcion)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->get();
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function responderAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];
			$my_id = Auth::user()->id;
			$requests = Requests::where('id', '=', $item)->paginate(10);
			foreach ($requests as $key) {
				$item_last = $key->user->id;
			}

			$previus_requests = Requests::where('user_id', '=', $item_last)->get();
			$TypeRequests 			= DB::table('Type_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$descripcion 			= DB::table('descripcion')->get();



			return View::make('containers.ajaxResponder')
			->with('requests',$requests)
			->with('previus_requests',$previus_requests)
			->with('TypeRequests',$TypeRequests)
			->with('processes',$processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('especifique',$especifique)
			->with('state_requests',$state_requests)
			->with('description',$descripcion)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->get();
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function respMasivo(){
		$item = $_POST['id'];
		echo $item;
		
	echo "hola";
	}
}

?>