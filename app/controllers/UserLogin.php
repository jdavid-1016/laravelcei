<?php 	
/**
* 	
*/
class UserLogin extends BaseController
{
	
	public function user()
	{
		//get POST data
		$userdata = array(
			'email_institutional' => Input::get('email'),
			'password' => Input::get('password'),
            'status' => 1
		);
                
                $remember = (Input::has('remember')) ? true : false;
                
		if(Auth::attempt($userdata, $remember))
		{                    
            
			//we are now logged in, go to admin
			return Redirect::to('informacion');
		}
		else{
			return Redirect::to('/')->with('login_errors',true);
		}
	}
        
        public function comprobar()
	{
		//get POST data
                $correo_institucional = Input::get('email');
                //$bandera = Users::find($correo_institucional);
                $bandera = DB::select('SELECT * FROM users WHERE email_institutional = ?', array($correo_institucional));
                if($bandera){
                    echo "1";
                }else{
                    echo "2";
                }
	}
        
        public function recuperar()
	{
		//get POST data
                $correo_institucional = Input::get('emailr');
                $passw = DB::select('SELECT document FROM users WHERE email_institutional = ?', array($correo_institucional));
                $pass = Hash::make($passw[0]->document);
                $editado = DB::update('UPDATE users SET password = ? WHERE email_institutional = ? ', array( $pass, $correo_institucional));
                if($editado){
                    
                    $fromEmail = "Indoamericana@indoamericana.edu.co";
                    $fromName = "Indoamericana";
                    
                    $data= array(
                        'email' => $correo_institucional,
                        'pass' => $passw[0]->document
                    );
                    
                Mail::send('emails.template', $data, function ($message) use ($fromName, $fromEmail){
                $message->subject('Recuperacion de contraseña');
                $message->to(Input::get('emailr'));
                $message->from($fromEmail, $fromName);
                });

                    return Redirect::to('/')->with('recuperar_errors',true);
                }else{
                    return Redirect::to('/')->with('recuperar_errors',false);
                }
	}
}
?>