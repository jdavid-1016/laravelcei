<link href="{{  URL::to("assets/css/pages/profile.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Mi Perfil
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Intranet</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> Mi Perfil</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row profile">
                <div class="col-md-12">
                    <!--BEGIN TABS-->
                    <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1_1" data-toggle="tab">Perfil</a></li>
                            <li><a href="#tab_1_3" data-toggle="tab">Editar Perfil</a></li>                     
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul class="list-unstyled profile-nav">
                                            <li><img src="{{ URL::to($perfil[0]->img_profile)}}" class="img-responsive" alt="" />                                     
                                            </li>
                                            <li><a href="proceso/{{ $perfil[0]-> id_proceso}}">{{ $perfil[0]-> proceso}}</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-7 profile-info">
                                                <h1>{{ $perfil[0]-> name." ".$perfil[0]-> name2." ".$perfil[0]-> last_name." ".$perfil[0]-> last_name2}}</h1>
                                                <ul class="list-unstyled">
                                                    <li><i class="icon-briefcase"></i> {{ $perfil[0]-> profile}}</li>
                                                    <li><i class="icon-headphones"></i>{{ Auth::user()->cell_phone }}</li>
                                                    <li><i class="icon-home"></i>{{ Auth::user()->phone }}</li>
                                                    <li><i class="icon-map-marker"></i>{{ Auth::user()->address }}</li>
                                                    <li><i class="icon-envelope"></i>{{ Auth::user()->email }}</li>
                                                </ul>
                                            </div>
                                            <!--end col-md-8-->
                                            <div class="col-md-5">
                                                <div class="portlet sale-summary">
                                                    <div class="portlet-title">
                                                        <div class="caption">Informacion Laboral</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <span class="sale-info">Extension <i class="icon-headphones"></i></span> 
                                                                <span class="sale-num">{{ $perfil[0]-> extension}}</span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">Email <i class="icon-envelope"></i></span>
                                                                <span class="sale-num">{{ Auth::user()->email_institutional }}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end col-md-4-->
                                        </div>
                                        <!--end row-->                                        
                                    </div>
                                </div>
                            </div>
                            <!--tab_1_2-->
                            <div class="tab-pane" id="tab_1_3">
                                <div class="row profile-account">
                                    <div class="col-md-3">
                                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                                            <li class="active">
                                                <a data-toggle="tab" href="#tab_1-1">
                                                    <i class="icon-user"></i> 
                                                    Información personal
                                                </a> 
                                                <span class="after"></span>                                    
                                            </li>                                 
                                            <li ><a id="redestuto" data-toggle="tab" href="#tab_2-2"><i class="icon-thumbs-up"></i> Redes Sociales</a></li>
                                            <li ><a id="passtuto" data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Cambiar contraseña</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="tab-content">
                                            <div id="tab_1-1" class="tab-pane active" >
                                                <div class="form-body">
                                                    <form>

                                                        <div class="col-md-5">
                                                            <div class="form-group"> 
                                                                <label class="control-label">Primer Nombre</label>
                                                                <div class="input-group">
                                                                    <input name="name" class="form-control" type="text" value="{{ $perfil[0]-> name}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Segundo Nombre</label>
                                                                <div class="input-group">
                                                                    <input name="sname" class="form-control" type="text" value="{{ $perfil[0]-> name2}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label class="control-label">Primer Apellido</label>
                                                                <div class="input-group">
                                                                    <input name="last_name" class="form-control" type="text" value="{{ $perfil[0]-> last_name}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Segundo Apellido</label>
                                                                <div class="input-group">
                                                                    <input name="slast_name" class="form-control" type="text" value="{{ $perfil[0]-> last_name2}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">   
                                                            <div class="form-group">
                                                                <label class="control-label editable">Número Celular</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ $perfil[0]-> cell_phone}}" class="form-control" name="celular" id="celular">
                                                                    <span class="input-group-addon"><i class="icon-mobile-phone"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label editable">Teléfono residencia</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->phone }}" class="form-control" name="telefono" id="telefono">
                                                                    <span class="input-group-addon"><i class="icon-phone"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>   

                                                        <div class="col-md-5">   
                                                            <div class="form-group">
                                                                <label class="control-label">Correo electrónico</label>
                                                                <div class="input-group">
                                                                    <input name="email_institutional" class="form-control" type="text" value="{{ $perfil[0]-> email_institutional}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label editable">Correo electrónico alternativo</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->email }}" class="form-control" name="email" id="email">
                                                                    <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5"> 
                                                            <div class="form-group">
                                                                <label class="control-label editable">Dirección de residencia</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->address }}" class="form-control" name="direccion" id="direccion">
                                                                    <span class="input-group-addon"><i class="icon-map-marker"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Cargo</label>
                                                                <div class="input-group">
                                                                    <input name="profile" class="form-control" type="text" value="{{ $perfil[0]-> profile}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-briefcase"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">
                                                            <div class="margiv-top-10">
                                                                <button type="button" onclick="actualizar()" class="btn btn-success">Actualizar</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            
                                            <div id="tab_2-2" class="tab-pane">
                                                <form id="formredes">
                                                <div class="form-group">
                                                    <label class="control-label">LinkedIn</label>
                                                    <div class="input-icon">
                                                            <i class=" icon-linkedin"></i>
                                                            <input type="text" value="{{ Auth::user()->linked }}" placeholder="https://co.linkedin.com/" class="form-control" name="linked" id="linked">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Google Plus</label>
                                                    <div class="input-icon">
                                                            <i class=" icon-google-plus"></i>
                                                            <input type="text" value="{{ Auth::user()->google }}" placeholder="https://plus.google.com/" class="form-control" name="google" id="google">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Facebook</label>
                                                    <div class="input-icon">
                                                            <i class="icon-facebook"></i>
                                                            <input type="text" value="{{ Auth::user()->facebook }}" placeholder="https://www.facebook.com/" class="form-control" name="facebook" id="facebook">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Twitter</label>
                                                    <div class="input-icon">
                                                            <i class="icon-twitter"></i>
                                                            <input type="text" value="{{ Auth::user()->twitter }}" placeholder="https://twitter.com/" class="form-control" name="twitter" id="twitter">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Skype</label>
                                                    <div class="input-icon">
                                                            <i class="icon-skype"></i>
                                                            <input type="text" value="{{ Auth::user()->skype }}" placeholder="Usuario" class="form-control" name="skype" id="skype">
                                                    </div>                                                    
                                                </div>
                                                <div class="margin-top-10">
                                                    <a class="btn btn-success" onclick="redes()">Guardar</a>
                                                </div>
                                                    </form>

                                            </div>
                                            
                                            <div id="tab_3-3" class="tab-pane">
                                                <form id="formpass">
                                                <div class="form-group">
                                                    <label class="control-label">Contraseña actual</label>
                                                    <input type="password" class="form-control" id="actual" required/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nueva contraseña</label>
                                                    <input type="password" class="form-control" id="nueva" required/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Re-ingresar nueva contraseña</label>
                                                    <input type="password" class="form-control" id="renueva" required/>
                                                </div>
                                                <div class="margin-top-10">
                                                    <a class="btn btn-success" onclick="cambiarpass()">Cambiar Contraseña</a>
                                                </div>
                                                    </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-md-9-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END TABS-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
   
    @if(Auth::user()->tutorial==0)
  <div class="opacidad">
      <div class="padre" id="padre">
          
          <div class="alert alert-block alert-info fade in inicio" id="paso1">
                        <h3 class="alert-heading" style="text-align:center;"><b>Perfil</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           En tu perfil podras cambiar la informacion basica que sera mostrada a los demas usuarios.
                        </p>
                        <p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/perfil.png") }}" style="width: 100%; height: 25%;">
                        </p>
                        </p>
                        </br>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(1)">Siguiente</a>
                        </p>
                     </div>
          
                     <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso2">
                        <h3 class="alert-heading" style="text-align:center;"><b>Redes Sociales</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Aca podras compartir todas tus redes sociales con los demas usuarios.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/redes.png") }}" style="width: 100%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(2)">Siguiente</a>
                        </p>
                     </div>

                     <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso3">
                        <h3 class="alert-heading" style="text-align:center;"><b>Olvido de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           En caso de olvidar tu contraseña y no poder iniciar sesion, podras recuperar tu cuenta facilmente.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/olvido.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 70%; height: 50%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
                        </p>
                     </div>
          
                    <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso4">
                        <h3 class="alert-heading" style="text-align:center;"><b>Olvido de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Solo sera necesario indicar tu correo corporativo y una nueva contraseña llegara a tu bandeja de entrada. 
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/olvido2.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 60%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(4)">Siguiente</a>
                        </p>
                     </div>
          
                    <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso5">
                        <h3 class="alert-heading" style="text-align:center;"><b>Cambio de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Para disfrutar de la aplicacion correctamente te invitamos a que cambies tu contraseña por defecto, por una mas segura.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/cambio.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 90%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(5)">Finalizar</a>
                        </p>
                     </div>
   </div>
</div>
    @endif
    <script>
function Siguiente(id){
         switch (id) {
                case 1:                
                $('#paso1').fadeOut(0);
                $('#paso2').fadeIn(0);
                $('#redestuto').click();
                    break;

                case 2:                
                $('#passtuto').click();
                $('#paso2').fadeOut(0);
                $('#paso3').fadeIn(0);
                break;

                case 3:
                $('#paso3').fadeOut(0);
                $('#paso4').fadeIn(0);
                break;
                
                case 4:
                $('#paso4').fadeOut(0);
                $('#paso5').fadeIn(0);
                break;
                
                case 5:
                    $.ajax({
                            type: "GET",
                            url: "actTutorial",
                            data: { cont:"1"}
                            })
                            .done(function( data ) {
                            if(parseInt(data)==2){
                            alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
                            }
                        });
                $('#paso5').fadeOut(0);
                $('.opacidad').fadeOut(1000);
                break;
             }
      }
        </script>