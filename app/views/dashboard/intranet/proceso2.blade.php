<link href="{{  URL::to("assets/css/pages/about-us.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">  
    
    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">          
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Gestión seguridad operacional
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="#">Intranet</a><i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="#"> Gestión seguridad operacional</a>
               </li>
            </ul>
         </div>
      </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>PROCESO SMS9</div>
                        </div>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">Información General</a></li>
                                <li><a id="documentacion" href="#tab_1_2" data-toggle="tab" dir="">Documentación</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">

                                    <div class="row margin-bottom-30">
                                        <div class="col-md-4">
                                            <div class="meet-our-team">
                                                <h3><small><b></b></small></h3>
                                                <img src="{{ URL::to("assets/img/profile/default.jpg")}}" alt="" class="img-responsive"/>
                                                <div class="team-info">
                                                    <p><b>Ext.</b>  <br /><b>Email:</b> </p>                                                    
                                                    <ul class="social-icons social-icons-color pull-right">
                                                        <li><a href="#" data-original-title="twitter" class="twitter"></a></li>
                                                        <li><a href="#" data-original-title="facebook" class="facebook"></a></li>
                                                        <li><a href="#" data-original-title="linkedin" class="linkedin"></a></li>
                                                        <li><a href="#" data-original-title="Goole Plus" class="googleplus"></a></li>
                                                        <li><a href="#" data-original-title="skype" class="skype"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h3>&nbsp;&nbsp;&nbsp;&nbsp;Reseña</h3><br />
                                            <blockquote class="hero">
                                                <div class="scroller" style="height:550px" data-rail-color="red" data-handle-color="black">
                                                <p style="text-align:justify">La CORPORACIÓN EDUCATIVA INDOAMERICANA, Centro de Instrucción certificado ante la Aeronáutica Civil, consciente de preservar la salud, la propiedad, la seguridad y el medio ambiente se compromete a mantener un sistema que asegure la mitigación de las situaciones de riesgo, mediante la promoción de seguridad, la asignación de recursos necesarios y el cumplimiento de la normatividad Nacional e Internacional. 

GESTIÓN SEGURIDAD OPERACIONAL.</p>
                                                <p>El proceso tiene como misi&oacute;n mitigar las situaciones de riesgo, que se&nbsp;puedan presentar en las actividades que tiene la Instituci&oacute;n en relaci&oacute;n&nbsp;con las operaciones a&eacute;reas, por&nbsp;lo tanto tiene como prop&oacute;sito mostrar&nbsp;las interacciones que el proceso de Gesti&oacute;n de la Seguridad&nbsp;Operacional, tiene con los dem&aacute;s procesos que conforman el Centro de&nbsp;Instrucci&oacute;n Aeron&aacute;utico CORPORACI&Oacute;N EDUCATIVA INDOAMERICANA&nbsp;LTDA, asegurando la disponibilidad de recursos e informaci&oacute;n que&nbsp;contribuyan a lograr los objetivos&nbsp;de la seguridad operacional&nbsp;mejorando las herramientas aplicadas para la identificaci&oacute;n de peligros&nbsp;y mitigaci&oacute;n de riesgos.</p><p>&nbsp;</p><p><strong>Cargos que conforman el proceso:</strong></p><ul>	<li>Direcci&oacute;n Aeron&aacute;utica</li></ul><p>&nbsp;</p><p><strong><span style="line-height: 20.7999992370605px;">Principales actividades realizadas por el proceso:</span></strong></p><ul>	<li>Proyectar y ejecutar el plan de implementaci&oacute;n del sistema de&nbsp;gesti&oacute;n de la seguridad operacional.</li>	<li>Planear y ejecutar el plan de gesti&oacute;n anual del proceso.</li>	<li>Realizar el plan de implementaci&oacute;n del sistema de gesti&oacute;n de la&nbsp;seguridad operacional.</li>	<li>Establecer los elementos de la pol&iacute;tica objetivos de la seguridad&nbsp;operacional.</li>	<li>Establecer responsabilidades y personal clave para desarrollar el&nbsp;SMS.</li>	<li>Establecer las necesidades de documentaci&oacute;n del Sistema de gesti&oacute;n&nbsp;de la seguridad operacional.</li>	<li>Planear las Auditor&iacute;as Internas al sistema de seguridad operacional.</li>	<li>Programar el seguimiento y cierre de acciones correctivas y&nbsp;preventivas.</li>	<li>Identificar peligros, evaluar y mitigar los riesgos.</li>	<li>Gestionar los riesgos de la seguridad operacional.</li>	<li>Hacer seguimiento a las no conformidades detectadas mediante&nbsp;auditorias, evaluaciones de satisfacci&oacute;n, revisiones por la direcci&oacute;n,&nbsp;quejas y reclamos.</li>	<li>Realizar monitoreo, seguimiento y medici&oacute;n al rendimiento del&nbsp;Sistema de Gesti&oacute;n de la Seguridad Operacional.</li>	<li>Implementar acciones correctivas y preventivas como resultado de&nbsp;las auditor&iacute;as realizadas al sistema de gesti&oacute;n de la seguridad&nbsp;operacional.</li>	<li>Divulgar a todo el personal el sistema de gesti&oacute;n de la seguridad&nbsp;operacional.</li>	<li>Realizar medici&oacute;n de la efectividad del entrenamiento al personal.</li></ul>
                                            </div>
                                            </blockquote>
                                        </div>

                                    </div>
                                    <!--/row-->   
                                    <!-- Meer Our Team -->
                                    <div class="headline">
                                        <h3></h3>
                                    </div>
                                    <div class="portlet">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-group"></i>Integrantes del Proceso</div>
                                        </div>
                                        <div class="portlet-body"> 
                                            <div class="row thumbnails">
                                                <div class="scroller" style="height:500px" data-rail-color="red" data-handle-color="black">
                                                <div class="col-md-3">
                                                    <div class="meet-our-team">
                                                        <h3><small><b></b></small></h3>
                                                        <img src="{{ URL::to("assets/img/profile/default.jpg")}}" alt="" class="img-responsive" />
                                                        <div class="team-info">
                                                            <p><b>Ext.</b> <br /><b>Email:</b> </p>
                                                            <ul class="social-icons social-icons-color pull-right">
                                                                <li><a href="#" data-original-title="twitter" class="twitter"></a></li>
                                                                <li><a href="#" data-original-title="facebook" class="facebook"></a></li>
                                                                <li><a href="#" data-original-title="linkedin" class="linkedin"></a></li>
                                                                <li><a href="#" data-original-title="Goole Plus" class="googleplus"></a></li>
                                                                <li><a href="#" data-original-title="skype" class="skype"></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab_1_2">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="portlet" id="docnav">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-list-ul"></i>Navegación</div>
                                                </div>
                                                <div class="portlet-body">
                                                    <span id="listdocument" ></span>   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-7 ">
                                            <div class="portlet">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-file"></i>Documento</div>
                                                </div>
                                                <div class="portlet-body" id="docview">
                                                    <embed id="doc-container" src="">   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                <!--/thumbnails-->
                                <!-- //End Meer Our Team -->        
                                <!-- END PAGE CONTENT-->
                            </div>
                            <!-- END PAGE -->    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
    <!-- END CONTAINER -->