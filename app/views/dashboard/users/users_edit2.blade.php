<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="kaldsjf" ng-submit="adminSupport()">
  
      <input type="hidden" ng-model="support_id" value="{{$data->id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Editar Usuario</h4>
      </div>
     <div class="modal-body">
         <form role="form" action="envio" method="post">
             <div class="form-body">
                 <div class="form-group">
                     <label>Nombre</label>
                     <input type="text" class="form-control" name="name" required=""  ng-model="name" value="{{$data->name}}">
                 </div>
                 <div class="form-group">
                     <label>Segundo Nombre</label>
                     <input type="text" class="form-control" name="name2" required=""  ng-model="name2" value="{{$data->name2}}">
                 </div>
                 <div class="form-group">
                     <label>Primer apellido</label>
                     <input type="text" class="form-control" name="last_name" required=""  ng-model="last_name" value="{{$data->last_name}}">
                 </div>
                 <div class="form-group">
                     <label>Segundo apellido</label>
                     <input type="text" class="form-control" name="last_name2" required=""  ng-model="last_name2" value="{{$data->last_name2}}">
                 </div>
                 <div class="form-group">
                     <label>Correo Institucional</label>
                     <input type="text" class="form-control" name="email_institutional" required=""  ng-model="email_institutional" value="{{$data->email_institutional}}">
                 </div>
                 <div class="form-group">
                     <label>Correo Personal</label>
                     <input type="text" class="form-control" name="email" required=""  ng-model="email" value="{{$data->email}}">
                 </div>
                 <div class="form-group">
                     <label>Celular</label>
                     <input type="text" class="form-control" name="cell_phone" required=""  ng-model="cell_phone" value="{{$data->cell_phone}}">
                 </div>
                 <div class="form-group">
                     <label>Teléfono</label>
                     <input type="text" class="form-control" name="phone" required=""  ng-model="phone" value="{{$data->phone}}">
                 </div>
                 <div class="form-group">
                     <label>direccion</label>
                     <input type="text" class="form-control" name="phone" required=""  ng-model="address" value="{{$data->address}}">
                 </div>

                 <div class="form-group">
                     <label>Tipo Documento</label>
                     <select class="form-control" name="type_document" required=""  ng-model="type_document">
                         <option value=""></option>                        
                        <option value="CC" @if($data->type_document == "CC") selected="" @endif>Cedula de Ciudadania</option>
                         <option value="TI" @if($data->type_document == "TI") selected="" @endif>Tarjeta de Identidad</option>
                         <option value="CE" @if($data->type_document == "CE") selected="" @endif>Cedula de Extanjería</option>
                     </select>
                 </div>
                 <div class="form-group">
                     <label>N° Documento</label>
                     <input type="text" class="form-control" name="document" required=""  ng-model="document" value="{{$data->document}}">
                 </div>
                 <div class="form-group">
                     <label>Ciudad {{$data->name}}</label>
                     <select class="form-control" name="city_id" required=""  ng-model="city_id">
                     @foreach($cities as $city)
                        @if($data->city_id == $city->id)
                            <option ng-repeat="city in cityes" value="{{$city->id}}" selected="">{{$city->name}}</option>
                        @else
                            <option ng-repeat="city in cityes" value="{{$city->id}}" >{{$city->name}}</option>
                        @endif
                     @endforeach
                         
                     </select>
                 </div>
                 @foreach ($data->Profiles as $perfil)
                 <?php 
                  $perfil_user = $perfil->id; 
                  $process_user = $perfil->processes_id; 
                ?>
                 @endforeach
                 <div class="form-group">
                     <label>Proceso: </label>
                     <select id="edit_process" class="form-control" name="proceso_perfil">
                     @foreach($processes as $process)
                        <option value='{{$process->id}}' @if($process_user == $process->id) selected="" @endif>{{$process->name}}</option>
                     @endforeach 
                     </select>
                 </div>
                 <div class="form-group" id="perfil">
                     <label>Cargo:</label>
                      <select id="profile" class="form-control" name="profile_user" ng-model="profile_user">
                      @foreach($profiles as $profile)      
                          <option value='{{$profile->id}}' @if($perfil_user == $profile->id) selected="" @endif>{{$profile->profile}}</option>
                      @endforeach 
                      </select>
                 </div>
             </div>
     </div>   
      <div class="modal-footer">
         <button type="submit" class="btn btn-success">Guardar</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
  
   </form>   
   </div>
</div>