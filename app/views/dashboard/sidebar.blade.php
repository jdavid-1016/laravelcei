<!--BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse" style="position:fixed">
   <!-- BEGIN SIDEBAR MENU -->        
   <ul class="page-sidebar-menu">
      <li>
         <form class="search-form search-form-sidebar" role="form">
            <div class="input-icon right">
               <i class="icon-search"></i>
               <input type="text" class="form-control input-medium input-sm" placeholder="Search...">
            </div>
         </form>
      </li>
      <li>
         <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
         <div class="sidebar-toggler"></div>
         <div class="clearfix"></div>
         <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
      </li>
      <!-- <li class="start  ">
         <a href="{{URL::to("informacion")}}">
         <i class="icon-home"></i> 
         <span class="title">Dashboard</span>
         <span class="selected"></span>
         </a>
      </li> -->
      <?php
//      $bandera="";
//                foreach ($user->profiles as $perfil){
//                foreach($perfil->submenus as $submenu){
//                           
//                $menu = Menus::find($submenu->menu_id);
//                if($bandera != $menu){
//                           echo $menu->menu."<br>";
//                           $bandera=$menu;
//                }
//                
//                          echo $submenu->submenu.'<br>';
//                           
//                }
//                
//                }
      ?>
      
      <?php
      $bandera="";
      $cont=0;
      ?>
      @foreach ($user->Profiles as $perfil)
      <?php
      $perfil_id = $perfil->id;      
      $sql = "SELECT * FROM profiles_submenus, submenus, menus where profiles_id = '$perfil_id' and submenus.id = submenus_id and menus.id = menu_id order by submenus.menu_id, submenus.id";
      $consulta = DB::select($sql);
        ?>
         @foreach($consulta as $submenu)
         <?php
            $menu = $submenu->menu_id;
         ?>
            @if($bandera != $menu)
               @if($cont !=0)
                     </ul>
                  </li>
            @endif
         <?php
            $cont=1;
         ?>
            @if($menu_activo == $submenu->menu)
            <li class="start active">
            @else
            <li class="">
            @endif            
            <a href="javascript:;">
         <i class="{{$submenu->icon}}"></i> 
         <span class="title">{{$submenu->menu}}</span>
         <span class="arrow "></span>
         </a>
            <ul class="sub-menu">
                           <?php
                           $bandera=$menu;
                           ?>
                @endif
                @if($submenu_activo == $submenu->submenu)
            <li class="active">
            @else
            <li class="">
            @endif
               <a href="{{URL::to($submenu->link)}}">
                   @if($submenu->submenu == "Administrar Solicitudes")
                   @if($notiadm > 0)
                   <span class="badge badge-important" id="{{ $submenu->id }}">{{ $notiadm }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id }}">0</span>
                   @endif
                   @endif
                   @if($submenu->submenu == "Atender Solicitudes")
                   @if($notiate > 0)
                   <span class="badge badge-important" id="{{ $submenu->id }}">{{ $notiate }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id }}">0</span>
                   @endif
                   @endif
                  {{$submenu->submenu}}</a>
            </li>
                @endforeach                
                @endforeach
       </ul>
        </li>
      
       <!-- <li class="">
         <a href="javascript:;">
         <i class="icon-cogs"></i> 
         <span class="title">Page Layouts</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="layout_language_bar.html">
               <span class="badge badge-important">new</span>Language Switch Bar</a>
            </li>
            <li >
               <a href="layout_disabled_menu.html">
               Disabled Menu Links</a>
            </li>
            <li >
               <a href="layout_sidebar_fixed.html">
               Sidebar Fixed Page</a>
            </li>
            <li >
               <a href="layout_sidebar_closed.html">
               Sidebar Closed Page</a>
            </li>
            <li >
               <a href="layout_blank_page.html">
               Blank Page</a>
            </li>
            <li >
               <a href="layout_boxed_page.html">
               Boxed Page</a>
            </li>
            <li >
               <a href="layout_boxed_not_responsive.html">
               Non-Responsive Layout</a>
            </li>
            <li >
               <a href="layout_ajax.html">
               Content Loading via Ajax</a>
            </li>
         </ul>
      </li> -->
     <!-- <li class="">
         <a href="javascript:;">
         <i class="icon-bookmark-empty"></i> 
         <span class="title">UI Features</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="ui_general.html">
               General</a>
            </li>
            <li >
               <a href="ui_buttons.html">
               Buttons</a>
            </li>
            <li >
               <a href="ui_typography.html">
               Typography</a>
            </li>
            <li >
               <a href="ui_modals.html">
               Modals</a>
            </li>
            <li >
               <a href="ui_extended_modals.html">
               Extended Modals</a>
            </li>
            <li >
               <a href="ui_tabs_accordions_navs.html">
               Tabs, Accordions & Navs</a>
            </li>
            <li >
               <a href="ui_toastr.html">
               <span class="badge badge-warning">new</span>Toastr Notifications</a>
            </li>
            <li >
               <a href="ui_tree.html">
               Tree View</a>
            </li>
            <li >
               <a href="ui_nestable.html">
               Nestable List</a>
            </li>
            <li >
               <a href="ui_ion_sliders.html">
               <span class="badge badge-important">new</span>Ion Range Sliders</a>
            </li>
            <li >
               <a href="ui_noui_sliders.html">
               <span class="badge badge-success">new</span>NoUI Range Sliders</a>
            </li>
            <li >
               <a href="ui_jqueryui_sliders.html">
               jQuery UI Sliders</a>
            </li>
            <li >
               <a href="ui_knob.html">
               Knob Circle Dials</a>
            </li>
         </ul>
      </li>
      <li class="">
         <a href="javascript:;">
         <i class="icon-table"></i> 
         <span class="title">Form Stuff</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="form_controls.html">
               Form Controls</a>
            </li>
            <li >
               <a href="form_layouts.html">
               Form Layouts</a>
            </li>
            <li >
               <a href="form_component.html">
               Form Components</a>
            </li>
            <li >
               <a href="form_editable.html">
               <span class="badge badge-warning">new</span>Form X-editable</a>
            </li>
            <li >
               <a href="form_wizard.html">
               Form Wizard</a>
            </li>
            <li >
               <a href="form_validation.html">
               Form Validation</a>
            </li>
            <li >
               <a href="form_image_crop.html">
               <span class="badge badge-important">new</span>Image Cropping</a>
            </li>
            <li >
               <a href="form_fileupload.html">
               Multiple File Upload</a>
            </li>
            <li >
               <a href="form_dropzone.html">
               Dropzone File Upload</a>
            </li>
         </ul>
      </li>
      <li class="">
         <a href="javascript:;">
         <i class="icon-sitemap"></i> 
         <span class="title">Pages</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="page_portfolio.html">
               <span class="badge badge-warning badge-roundless">new</span>Portfolio</a>
            </li>
            <li >
               <a href="page_blog.html">
               Blog</a>
            </li>
            <li >
               <a href="page_blog_item.html">
               Blog Post</a>
            </li>
            <li >
               <a href="page_about.html">
               About Us</a>
            </li>
            <li >
               <a href="page_contact.html">
               Contact Us</a>
            </li>
            <li >
               <a href="page_calendar.html">
               <span class="badge badge-important">14</span>Calendar</a>
            </li>
            <li >
               <a href="page_profile.html">
               User Profile</a>
            </li>
            <li >
               <a href="page_faq.html">
               FAQ</a>
            </li>
            <li >
               <a href="page_inbox.html">
               <span class="badge badge-important">4</span>Inbox</a>
            </li>
            <li >
               <a href="page_invoice.html">
               Invoice</a>
            </li>
            <li >
               <a href="page_pricing_table.html">
               Pricing Tables</a>
            </li>
            <li >
               <a href="page_404_option1.html">
               404 Page Option 1</a>
            </li>
            <li >
               <a href="page_404_option2.html">
               404 Page Option 2</a>
            </li>
            <li >
               <a href="page_500_option1.html">
               500 Page Option 1</a>
            </li>
            <li >
               <a href="page_500_option2.html">
               500 Page Option 2</a>
            </li>
         </ul>
      </li>
      <li class="">
         <a href="javascript:;">
         <i class="icon-th"></i> 
         <span class="title">Data Tables</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="<?= URL::to('users'); ?>">
               Users</a>
            </li>
            <li >
               <a href="table_responsive.html">
               Responsive Tables</a>
            </li>
            <li >
               <a href="table_managed.html">
               Managed Tables</a>
            </li>
            <li >
               <a href="table_editable.html">
               Editable Tables</a>
            </li>
            <li >
               <a href="table_advanced.html">
               Advanced Tables</a>
            </li>
         </ul>
      </li>
      
      <li class="">
         <a href="javascript:;">
         <i class="icon-map-marker"></i> 
         <span class="title">Maps</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li >
               <a href="maps_google.html">
               Google Maps</a>
            </li>
            <li >
               <a href="maps_vector.html">
               Vector Maps</a>
            </li>
         </ul>
      </li> -->
      <!-- <li class="">
         <a href="users">
         <i class="icon-user"></i> 
         <span class="title">Usuarios</span>
         </a>
      </li> -->
      
      <!-- <li>
         <a href="javascript:;">
         <i class="icon-folder-open"></i> 
         <span class="title">4 Level Menu</span>
         <span class="arrow "></span>
         </a>
         <ul class="sub-menu">
            <li>
               <a href="javascript:;">
               <i class="icon-cogs"></i> 
               Item 1
               <span class="arrow"></span>
               </a>
               <ul class="sub-menu">
                  <li>
                     <a href="javascript:;">
                     <i class="icon-user"></i>
                     Sample Link 1
                     <span class="arrow"></span>
                     </a>
                     <ul class="sub-menu">
                        <li><a href="#"><i class="icon-remove"></i> Sample Link 1</a></li>
                        <li><a href="#"><i class="icon-pencil"></i> Sample Link 1</a></li>
                        <li><a href="#"><i class="icon-edit"></i> Sample Link 1</a></li>
                     </ul>
                  </li>
                  <li><a href="#"><i class="icon-user"></i>  Sample Link 1</a></li>
                  <li><a href="#"><i class="icon-external-link"></i>  Sample Link 2</a></li>
                  <li><a href="#"><i class="icon-bell"></i>  Sample Link 3</a></li>
               </ul>
            </li>
            <li>
               <a href="javascript:;">
               <i class="icon-globe"></i> 
               Item 2
               <span class="arrow"></span>
               </a>
               <ul class="sub-menu">
                  <li><a href="#"><i class="icon-user"></i>  Sample Link 1</a></li>
                  <li><a href="#"><i class="icon-external-link"></i>  Sample Link 1</a></li>
                  <li><a href="#"><i class="icon-bell"></i>  Sample Link 1</a></li>
               </ul>
            </li>
            <li>
               <a href="#">
               <i class="icon-folder-open"></i>
               Item 3
               </a>
            </li>
         </ul>
      </li>
      <li class="last ">
         <a href="login.html">
         <i class="icon-user"></i> 
         <span class="title">Login</span>
         </a>
      </li> -->
   </ul>
   <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR