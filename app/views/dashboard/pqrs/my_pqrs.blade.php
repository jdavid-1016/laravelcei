<div class="page-content">
   <!-- BEGIN PAGE CONTENT-->
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Mis PQRS</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="#tab-agregar_pqr" data-toggle="tab">Mis PQRS</a></li>
                  <li class=""><a href="#tab-mis_pqrs" data-toggle="tab">Agregar PQR</a></li>
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade in" id="tab-mis_pqrs">
                     
                    <div class="row">
                       <form class="form-horizontal col-md-9" role="form" action="pqrs/new" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                          <div class="form-body">
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Fecha y Hora:</label>
                                <div class="col-md-9">
                                   <?php date_default_timezone_set('America/Bogota') ?>
                                   <input type="text" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Tipo de Solicitud:</label>
                                <div class="col-md-9">
                                   <select  class="form-control" required="" name="type_requests_id">
                                   <option></option>
                                   @if($TypeRequests)
                                   @foreach($TypeRequests as $TypeRequests)
                                      <option value="{{$TypeRequests->id}}">{{$TypeRequests->name}}</option>
                                   @endforeach
                                   @endif
                                   </select>
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Proceso Involucrado:</label>
                                <div class="col-md-9">
                                   <select  class="form-control" name="process_id" required="" id="select1" onchange="cargarSelect2(this.value);">
                                      <option></option>
                                   @if($process)
                                   @foreach($process as $process)
                                      <option value="{{$process->id}}">{{$process->name}}</option>
                                   @endforeach
                                   @endif
                                   </select>
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Detalle:</label>
                                <div class="col-md-9">
                                   <select  class="form-control" required="" id="select2" disabled="" name="detalle_id">
                                      <option>Selecciona una opción</option>
                                   </select>
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Tipo de Usuario:</label>
                                <div class="col-md-9">
                                   <select  class="form-control" required="" name="type_users_id">
                                      <option></option>
                                   @if($TypeUsers)
                                   @foreach($TypeUsers as $typeUsers)
                                      <option value="{{$typeUsers->id}}">{{$typeUsers->name}}</option>
                                   @endforeach
                                   @endif
                                   </select>
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Motivo: </label>
                                <div class="col-md-9">
                                   <input type="text" class="form-control"  placeholder="" required="" name="title">
                                </div>
                             </div>
                             <div class="form-group">
                                <label  class="col-md-3 control-label">Descripción</label>
                                <div class="col-md-9">
                                   <textarea class="form-control" rows="3" required="" name="description"></textarea>
                                </div>
                             </div>
                             <div class="form-group">
                                <label for="exampleInputFile" class="col-md-3 control-label">Soporte de PQR</label>
                                <div class="col-md-9">
                                   <input type="file" id="exampleInputFile" name="support">
                                   <p class="help-block">Solo se aceptan imágenes JPG o GIF</p>
                                </div>
                             </div>
                             
                          </div>
                          <div class="form-actions fluid">
                             <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success">Enviar PQR</button>
                                <button type="button" class="btn btn-default">Cancel</button>                              
                             </div>
                          </div>
                       </form>
                    </div>

                  </div>

                  <div class="tab-pane fade active in" id="tab-agregar_pqr">
                    <div class="col-md-12">
                       <div class="portlet">
                          <div class="portlet-title">
                             <div class="caption"><i class="icon-bell"></i>Mis PQRS</div>
                             <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                                <a href="javascript:;" class="remove"></a>
                             </div>
                          </div>
                          <div class="portlet-body">
                             <div class="table-responsive">
                                <table class=" table-striped table-bordered table-advance table-hover text-center">
                                   <thead>
                                      <tr>
                                          
                                         <th class="text-center">N° PQR</th>
                                         <th class="text-center" style="min-width:150px"> Motivo</th>
                                         <th class="text-center hidden-xs" > Descripción</th>
                                         <th class="text-center" style="min-width:140px"> Usuario</th>
                                         <th class="text-center"> Estado</th>
                                         <th class="text-center" style="min-width:140px"> Proceso</th>
                                         <th class="text-center" style="min-width:140px"> Tipo de alumno</th>
                                         <th class="text-center"> Tipo de pqr</th>
                                         <th class="text-center"> Acciones</th>
                                         
                                      </tr>
                                   </thead>
                                   <tbody>
                                    @if(isset($error))
                                     <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong>Error!</strong> {{$error}}.
                                     </div>
                                    @else
                                    @if($requests)
                                    <!-- $requests es la variable enviada desde el controlador -->
                                    @foreach($requests as $request)
                                    @if($request->stateRequests->name == 'En Curso' || $request->stateRequests->name == 'Finalizado')
                                       <tr >
                                       
                                         <td class="highlight" style="width:120px;">
                                         <i class='icon-sort-down'></i>
                                             <a class="accordion-toggle" data-toggle='collapse' data-parent='#accordion1' href='#collapse_{{$request->id}}'>PQR-00{{$request->id}}</a>
                                         </td>
                                         <td >{{$request->title}}</td>
                                         <td class="hidden-xs" style="padding:10px">{{$request->description}}</td>
                                         <td>
                                            {{$request->user->name}}
                                            {{$request->user->last_name}}
                                         </td>
                                         <td>
                                            @if($request->stateRequests->name == 'Pendiente')
                                            <span class="label label-danger">{{$request->StateRequests->name}}</span>
                                            @elseif($request->stateRequests->name == 'Finalizado')
                                            <span class="label label-success">{{$request->StateRequests->name}}</span>
                                            @else
                                            <span class="label label-info">{{$request->StateRequests->name}}</span>
                                            @endif
                                         </td>
                                         <td>{{$request->process->name}}</td>
                                         <td>{{$request->TypeUsers->name}}</td>
                                         <td>{{$request->TypeRequests->name}}</td>
                                         <td>
                                            @if($request->stateRequests->name == 'Finalizado')
                                            <a href="#" class="btn btn-sm btn-default"><i class="icon-edit"></i>Calificar</a>
                                            @else
                                            <a href="#" class="btn btn-sm btn-default disabled"><i class="icon-edit"></i>Calificar</a>
                                            @endif
                                         </td>
                                         
                                       </tr>
                                       <tbody id='collapse_{{$request->id}}' class="panel-collapse collapse panel-body">
                                          
                                       @foreach($traceabilitys as $traceability)
                                       @if($request->id == $traceability->requests_id)
                                          <tr>
                                             <td></td>
                                             <td></td>
                                             <td>
                                                <p class="text-info text-left" style="padding:10px;">
                                                   <strong> Usuario: </strong>{{$traceability->user->name}} {{$traceability->user->last_name}}<br>
                                                   <strong> Comentario: </strong>{{$traceability->coment}}<br>
                                                   <strong> Fecha de respuesta: </strong>{{$traceability->date}}<br>
                                                </p>
                                             </td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                             <td></td>
                                          </tr>
                                       @endif
                                       @endforeach
                                       </tbody>
                                    @else
                                       <tr>

                                          <td class="highlight" style="width:120px;">
                                              PQR-00{{$request->id}}
                                          </td>
                                          <td >{{$request->title}}</td>
                                          <td class="hidden-xs" style="padding:10px">{{$request->description}}</td>
                                          <td>
                                             {{$request->user->name}}
                                             {{$request->user->last_name}}
                                          </td>
                                          <td>
                                             @if($request->stateRequests->name == 'Pendiente')
                                             <span class="label label-danger">{{$request->StateRequests->name}}</span>
                                             @elseif($request->stateRequests->name == 'Finalizado')
                                             <span class="label label-success">{{$request->StateRequests->name}}</span>
                                             @else
                                             <span class="label label-info">{{$request->StateRequests->name}}</span>
                                             @endif
                                          </td>
                                          <td>{{$request->process->name}}</td>
                                          <td>{{$request->TypeUsers->name}}</td>
                                          <td>{{$request->TypeRequests->name}}</td>
                                          <td>
                                             @if($request->stateRequests->name == 'Finalizado')
                                             <a href="#" class="btn btn-sm btn-default"><i class="icon-edit"></i>Calificar </a>
                                             @else
                                             <a href="#" class="btn btn-sm btn-default disabled"><i class="icon-edit"></i>Calificar</a>
                                             @endif
                                          </td>
                                          
                                       </tr>
                                    @endif
                                    @endforeach
                                    <div style="margin:0 auto;">
                                       <?php echo $requests->links(); ?>
                                       </div>
                                    @endif
                                    @endif
                                   
                                   </tbody>

                                </table>
                                
                             </div>
                          </div>
                       </div>
                       
                    </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
         
      </div>
   </div>
   
   <!-- END PAGE CONTENT-->
</div>
   <script type="text/javascript">
   /**
    * Funcion que se ejecuta al seleccionar una opcion del primer select
    */
   function cargarSelect2(valor)
   {
       /**
        * Este array contiene los valores sel segundo select
        * Los valores del mismo son:
        *  - hace referencia al value del primer select. Es para saber que valores
        *  mostrar una vez se haya seleccionado una opcion del primer select
        *  - value que se asignara
        *  - testo que se asignara
        */
         

       var arrayValores=new Array(
         @if($detalle)
         @foreach($detalle as $detalle)
            new Array({{$detalle->process_id}},{{$detalle->id}},'{{$detalle->detalle}}'),
         @endforeach
         @endif
           
         new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
       );
       if(valor==0)
       {
           // desactivamos el segundo select
           document.getElementById("select2").disabled=true;
       }else{
           // eliminamos todos los posibles valores que contenga el select2
           document.getElementById("select2").options.length=0;
           
           // añadimos los nuevos valores al select2
           document.getElementById("select2").options[0]=new Option("Selecciona una opcion", "0");
           for(i=0;i<arrayValores.length;i++)
           {
               // unicamente añadimos las opciones que pertenecen al id seleccionado
               // del primer select
               if(arrayValores[i][0]==valor)
               {
                   document.getElementById("select2").options[document.getElementById("select2").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
               }
           }
           
           // habilitamos el segundo select
           document.getElementById("select2").disabled=false;
       }
   }


   
</script>
