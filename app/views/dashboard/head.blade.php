<?php
$url = $_SERVER['REQUEST_URI'];
$resultado = strpos($url, "proceso");
$valor = 0;
if($resultado !== FALSE){
    $valor = 1;
}
?>
<input type="hidden" id="fancyurl" value="<?php echo $valor; ?>">
<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
   <meta charset="utf-8" />
   <title>Indoamericana</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
   <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
   <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
   <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
   <!-- END PAGE LEVEL PLUGIN STYLES -->
   <!-- BEGIN THEME STYLES --> 
   <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="{{URL::to("favicon.ico")}}" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
   <!-- BEGIN HEADER -->   
   <div class="header navbar navbar-inverse navbar-fixed-top">
      <!-- BEGIN TOP NAVIGATION BAR -->
      <div class="header-inner">
         <!-- BEGIN LOGO -->  
         <a class="navbar-brand" href="">
         <img src="{{URL::to("assets/img/logo.png")}}" alt="logo" class="img-responsive" />
         </a>
         <!--<form class="search-form search-form-header" role="form" action="index.html" >
            <div class="input-icon right">
               <i class="icon-search"></i>
               <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
            </div>
         </form>-->
         <!-- END LOGO -->
         <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
         <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <img src="{{URL::to("assets/img/menu-toggler.png")}}" alt="" />
         </a> 
         <!-- END RESPONSIVE MENU TOGGLER -->
         <?php 
         $noti = Notifications::where('user_id' ,'=', Auth::user()->id)
         ->orderBy('id', 'desc')
         ->take('10')
         // ->skip('2')
         ->get();

         $noti2 = Notifications::where('user_id' ,'=', Auth::user()->id)
         ->where('estate' ,1)
         ->orderBy('id', 'desc')
         ->get();
         $num = count($noti2);



         function interval_date($init,$finish){
             //formateamos las fechas a segundos tipo 1374998435
             $diferencia = strtotime($finish) - strtotime($init);
          
             //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
             //floor devuelve el número entero anterior, si es 5.7 devuelve 5
             if($diferencia < 60){
                 $tiempo = floor($diferencia) . " Seg";
             }else if($diferencia > 60 && $diferencia < 3600){
                 $tiempo = floor($diferencia/60) .  ' Min';
             }else if($diferencia > 3600 && $diferencia < 86400){
                 $tiempo = floor($diferencia/3600) . " Horas";
             }else if($diferencia > 86400 && $diferencia < 2592000){
                 $tiempo = floor($diferencia/86400) . " Días";
             }else if($diferencia > 2592000 && $diferencia < 31104000){
                 $tiempo = floor($diferencia/2592000) . " Meses";
             }else if($diferencia > 31104000){
                 $tiempo = floor($diferencia/31104000) . " Años";
             }else{
                 $tiempo = "Error";
             }
             return $tiempo;
         }

          ?>
          @if($num == 0)
          <?php $class_noti = "display:none"; ?>
          @else
          <?php $class_noti = "" ?>
          @endif
         <!-- BEGIN TOP NAVIGATION MENU -->
         <ul class="nav navbar-nav pull-right">
            <!-- BEGIN NOTIFICATION DROPDOWN -->
            <li class="dropdown" id="header_notification_bar">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"data-close-others="true" id="icon_notifications">
               <i class="icon-warning-sign"></i>
               <span class="badge badge-danger" id="cont-not" style="{{$class_noti}}">{{$num}}</span>
               </a>
               <ul class="dropdown-menu extended notification lista_notificaciones">
                  <li>
                     <p>Notificaciones</p>
                  </li>
                  <li>
                     <ul class="dropdown-menu-list scroller" id="lista-not" style="height: 250px;">
                     @foreach($noti as $notification)
                        <?php $user = Users::find($notification->user_id1); ?>
                        @if($notification->estate == 1)
                            <?php $class = 'notificacion'; ?>
                        @else
                            <?php $class = 'nada' ?>
                        @endif

                        <?php 
                            $fecha = $notification->created_at;
                            $fecha_actual = date('Y-m-d H:i:s');
                            $fecha_t =  interval_date($fecha, $fecha_actual);
                        ?>
                     
                        <li>
                         <a href="{{$notification->link}}" class="{{$class}}" id="{{$notification->id}}" onclick="desactive_notification($(this).attr('id'));return false;">
                           <span class="label label-sm label-icon "><img alt class="top-avatar" src="{{ URL::to($user->img_min)}}"></span>
                           <strong>{{$user->name}} {{$user->last_name}}</strong>
                           {{$notification->description}}
                           <span class="time">Hace {{$fecha_t}}</span>

                           </a>
                        </li>

                     @endforeach
                        <!-- <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                           Server overloaded. 
                           <span class="time">15 mins</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-warning"><i class="icon-bell"></i></span>
                           Reboot failed.
                           <span class="time">22 mins</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-info"><i class="icon-bullhorn"></i></span>
                           System error.
                           <span class="time">40 mins</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                           Network overloaded 90%. 
                           <span class="time">2 hrs</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-danger"><i class="icon-bolt"></i></span>
                           Order canceled.
                           <span class="time">5 hrs</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-warning"><i class="icon-bell"></i></span>
                           Storage Server #4 not responding.
                           <span class="time">45 mins</span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="label label-sm label-icon label-info"><i class="icon-bullhorn"></i></span>
                           System Error.
                           <span class="time">55 mins</span>
                           </a>
                        </li> -->
                        <!-- <li>  
                           <a style="text-align:center" onclick="load_more_notifications()">Ver más </a>
                        </li> -->
                     </ul>
                  </li>
                  <li class="external">   
                     <a href="all_my_notifications">Ver todas las notificaciones <i class="icon-angle-right"></i></a>
                  </li>
               </ul>
            </li>
            <!-- END NOTIFICATION DROPDOWN -->

            <?php 
            $my_id = Auth::user()->id;

                $sql2 = "SELECT chat_id, count(*) as dato FROM `messages` WHERE users_id = '$my_id' or users_id1 = '$my_id' group by chat_id";
                $chats = DB::select($sql2);

                // foreach ($chats as $chat) {
                //     $chat_id = $chat->chat_id;
                //     $sql = "SELECT * FROM `messages` WHERE chat_id = '$chat_id'";
                //     $messages = DB::select($sql);
                   
                // }

                //$sql = "SELECT * FROM `messages` WHERE users_id = '$my_id' or users_id1 = '$my_id'";
                //$messages = DB::select($sql);

            ?>
            

            <!-- BEGIN INBOX DROPDOWN -->
            <li class="dropdown" id="header_inbox_bar">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                  data-close-others="true">
               <i class="icon-envelope"></i>
               <span class="badge badge-info">5</span>
               </a>
               <ul class="dropdown-menu extended inbox">
                  <li>
                     <p>You have 12 new messages</p>
                  </li>
                  <li>
                     <ul class="dropdown-menu-list scroller" style="height: 250px;">
                     @foreach($chats as $chat) 
                     <?php 
                        $chat_id = $chat->chat_id;
                        $sql = "SELECT * FROM `messages` WHERE chat_id = '$chat_id' order by id desc limit 1";
                        $messages = DB::select($sql);
                     ?>
                        @foreach($messages as $message)

                            <?php 
                                // $cahtid = $chat->chat_id;
                                // $sql = "SELECT * FROM `messages` WHERE chat_id  = '$cahtid' order by id desc limit 1";
                                // $messages = DB::select($sql);
                            ?>
                        
                            <?php 
                                $user = Users::find($message->users_id); 
                                $user_id = $user->id;
                                $class = 'nada';
                                if ($user_id == $my_id) {
                                    $style = "";
                                    $user = Users::find($message->users_id1);
                                }else{
                                    $style = "icon-share-alt";
                                    if($message->read == 0){
                                        $class = 'notificacion';
                                    }
                                }


                            ?>

                            
                                
                            
                            

                            <?php 
                                $fecha = $message->created_at;
                                $fecha_actual = date('Y-m-d H:i:s');
                                $fecha_t =  interval_date($fecha, $fecha_actual);
                            ?>
                        
                           <li>  
                              <a href="mensajes?chat_id={{$chat_id}}" class="{{$class}}">
                              <span class="photo"><img src="{{URL::to($user->img_min)}}" alt=""/></span>
                              <span class="subject">
                              <span class="from">{{$user->name}} {{$user->last_name}}</span>
                              <span class="time">{{$fecha_t}}</span>
                              </span>
                              <span class="message" {{$style}}>
                              <i class="{{$style}}"></i>
                              {{$message->message}}
                              </span>  
                              </a>
                           </li>
                        
                        @endforeach
                    @endforeach
                        <!-- <li>  
                           <a href="inbox.html?a=view">
                           <span class="photo"><img src="{{URL::to("assets/img/avatar3.jpg")}}" alt=""/></span>
                           <span class="subject">
                           <span class="from">Patrick Nilson</span>
                           <span class="time">11 mins</span>
                           </span>
                           <span class="message">
                           Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
                           auctor nibh...
                           </span>  
                           </a>
                        </li>
                        <li>  
                           <a href="inbox.html?a=view">
                           <span class="photo"><img src="{{URL::to("assets/img/avatar1.jpg")}}" alt=""/></span>
                           <span class="subject">
                           <span class="from">Nick</span>
                           <span class="time">2 hrs</span>
                           </span>
                           <span class="message">
                           Vivamus sed nibh auctor nibh congue nibh. auctor nibh
                           auctor nibh...
                           </span>  
                           </a>
                        </li>
                        <li>  
                           <a href="inbox.html?a=view">
                           <span class="photo"><img src="{{URL::to("assets/img/avatar2.jpg")}}" alt=""/></span>
                           <span class="subject">
                           <span class="from">Rose Wong</span>
                           <span class="time">3 hrs</span>
                           </span>
                           <span class="message">
                           Vivamus sed auctor 40% nibh congue nibh...
                           </span>  
                           </a>
                        </li>
                        <li>  
                           <a href="inbox.html?a=view">
                           <span class="photo"><img src="{{URL::to("assets/img/avatar3.jpg")}}" alt=""/></span>
                           <span class="subject">
                           <span class="from">Jhon Doe</span>
                           <span class="time">5 hrs</span>
                           </span>
                           <span class="message">
                           Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh
                           auctor nibh...
                           </span>  
                           </a>
                        </li> -->
                     </ul>
                  </li>
                  <li class="external">   
                     <a href="mensajes" >Ver todos <i class="icon-angle-right"></i></a>
                  </li>
               </ul>
            </li>
            <!-- END INBOX DROPDOWN -->
            <!-- BEGIN TODO DROPDOWN -->
            <!--<li class="dropdown" id="header_task_bar">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
               <i class="icon-ok"></i>
               <span class="badge badge-warning">5</span>
               </a>
               <ul class="dropdown-menu extended tasks">
                  <li>
                     <p>You have 12 pending tasks</p>
                  </li>
                  <li>
                     <ul class="dropdown-menu-list scroller" style="height: 250px;">
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Conquer v1.6 release</span>
                           <span class="percent">30%</span>
                           </span>
                           <span class="progress">
                           <span style="width: 10%;" class="progress-bar progress-bar-info" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">10% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Project Demo</span>
                           <span class="percent">65%</span>
                           </span>
                           <span class="progress progress-striped">
                           <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">65% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">iOS app release</span>
                           <span class="percent">98%</span>
                           </span>
                           <span class="progress">
                           <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">98% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Server maintenance</span>
                           <span class="percent">10%</span>
                           </span>
                           <span class="progress progress-striped">
                           <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">10% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Project status update</span>
                           <span class="percent">58%</span>
                           </span>
                           <span class="progress progress-striped">
                           <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">58% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Android app development</span>
                           <span class="percent">85%</span>
                           </span>
                           <span class="progress progress-striped">
                           <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">85% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                        <li>  
                           <a href="#">
                           <span class="task">
                           <span class="desc">Conquer v1.5 release</span>
                           <span class="percent">18%</span>
                           </span>
                           <span class="progress progress-striped">
                           <span style="width: 18%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                           <span class="sr-only">18% Complete</span>
                           </span>
                           </span>
                           </a>
                        </li>
                     </ul>
                  </li>
                  <li class="external">   
                     <a href="#">See all tasks <i class="icon-angle-right"></i></a>
                  </li>
               </ul>
            </li>-->
            <!-- END TODO DROPDOWN -->
            <li class="devider">&nbsp;</li>
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
               <img alt="" class="top-avatar" src="{{URL::to(Auth::user()->img_min)}}"/>
               <span class="username"> {{ Auth::user()->name }} {{ Auth::user()->last_name }} </span>
               <i class="icon-angle-down"></i>
               </a>
               <ul class="dropdown-menu">
                  <li><a href="{{URL::to("perfil")}}"><i class="icon-user"></i> Mi Perfil</a>
                  </li>                  
                  <li class="divider"></li>
                  </li>
                  <li><a href="{{URL::to("logout")}}"><i class="icon-key"></i> Log Out</a>
                  </li>
               </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
         </ul>
         <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END TOP NAVIGATION BAR -->
   </div>