   <!-- BEGIN FOOTER -->
   <input type="hidden" id="id_unico_loguin" value="<?php echo Auth::user()->id; ?>">
   <div class="footer">       
      <div class="footer-inner">
         2014 &copy; Indoamericana.
      </div>
      <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
      </div>
   </div>         
   <div id="sound">
       </div>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="assets/plugins/respond.min.js"></script>
   <script src="assets/plugins/excanvas.min.js"></script> 
   <![endif]-->   
   @if($submenu_activo == "Estadísticas")
   @else
     <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
   @endif
   <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
   <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/jquery.vmap.js")}}" type="text/javascript"></script>   
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
   <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
   // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
   <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
   <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
   <!-- END PAGE LEVEL SCRIPTS -->     
   <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
   <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
   <script src="{{ URL::to("js/app.js")}}"></script>
   <script src="{{ URL::to("js/validar.js")}}"></script>
   <script src="{{ URL::to("js/angular.min.js")}}"></script>
   <script src="{{ URL::to("js/documentacion.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->
  <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   <script>
      jQuery(document).ready(function() {
         App.init(); // initlayout and core plugins
         Portfolio.init();
         Index.init();
         Index.initJQVMAP(); // init index page's custom scripts
         Index.initCalendar(); // init index page's custom scripts
         Index.initCharts(); // init index page's custom scripts
         Index.initChat();
         Index.initMiniCharts();
         Index.initPeityElements();
         Index.initKnowElements();
         Index.initDashboardDaterange();         
         Tasks.initDashboardWidget();
         UITree.init();
         UIToastr.init();
         FormComponents.init();                      
      });
   </script>
   <script type="text/javascript">
   $(document).ready (function(){
       
       $('.myCarousel').carousel()
       
      $('.delete').click (function(){

         if (confirm("¿Esta seguro que desea eliminar un usuario?")) {
            var id= $(this).attr ("title");
            document.location.href='users/delete/'+id;
         }
      });

      $('[name="escalar"].escalar').click(function() {
        if($(this).is(':checked')) {
          alert('Se hizo check en el checkbox.');
        } else {
          alert('Se destildo el checkbox');
        }
      });                  
         
   });      
</script>
<script>
$(document).ready(function(){
   $('.edit').click(function(){

      $('[name=user]').val($(this).attr ('id'));

      var faction ="<?php echo URL::to('user/getuser/data'); ?>";

      var fdata = $('#val').serialize();
         $('#load').show();
      $.post(faction, fdata, function(json){
         if (json.success) {             
            $('#formEdit input[name="name_edit"]').val(json.name);
            $('#formEdit input[name="date_edit"]').val(json.date);
            $('#formEdit input[name="phone_edit"]').val(json.phone);
            $('#formEdit input[name="username_edit"]').val(json.username);
         }
      });
   });
}
</script>
   <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
