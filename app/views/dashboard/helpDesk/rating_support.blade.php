<div class="modal-dialog">
  <div class="modal-content"> 
    
    <form>
    <input type="hidden" value="{{$data->id}}" id="support_id">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"><i class="icon-laptop"></i> Evaluación del servicio de Help Desk</h4>
      </div>
      <div class="modal-body form-body">
        <div class="alert alert-block alert-info fade in">
          <p>Por favor, a continuación seleccione una puntuación para cada ítem. Recuerde que <span class="radio-slc rdo-success">5</span> es totalmente conforme
          y <span class="radio-slc rdo-danger">1</span> definitivamente inconforme.</p>
        </div>
        <div class="row">
          <div class="col-md-12">
          @foreach($questions as $question)
            <div class="form-group">
              <label class="control-label">{{$question->description}}</label>
              <div class="radio-list">
                <div class="radio-slc rdo-danger"><input type="radio" name="question_{{$question->id}}" value="1" id="question_{{$question->id}}"> 1</div>
                <div class="radio-slc rdo-middlea"><input type="radio" name="question_{{$question->id}}" value="2" id="question_{{$question->id}}"> 2</div>
                <div class="radio-slc rdo-warning"><input type="radio" name="question_{{$question->id}}" value="3" id="question_{{$question->id}}"> 3</div>
                <div class="radio-slc rdo-middleb"><input type="radio" name="question_{{$question->id}}" value="4" id="question_{{$question->id}}"> 4</div>
                <div class="radio-slc rdo-success"><input type="radio" name="question_{{$question->id}}" value="5" id="question_{{$question->id}}"> 5</div>
              </div>
            </div>
          @endforeach
            <div class="form-group">
              <label>Observaciones</label>
              <textarea name="comment" class="form-control" rows="3" id="comment" required></textarea>
            </div>
          </div>      
        </div> 
      </div>         
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="ratingSupport()">Guardar</button>
        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
    </form>
  </div>
</div>
