
<div class="page-content">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Atender Solicitudes</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="#tab_1_1" data-toggle="tab">Solicitudes</a></li>
                  <li class=""><a href="my_supports_asign_history" >Histórico</a></li>
                  <li class=""><a href="statistics_tec" >Estadisticas</a></li>
                  
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_respond_supports">
                        @include('dashboard.helpDesk.table_respond_supports')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  <div class="tab-pane fade" id="tab_1_2">
                     <div class="table-responsive" id="historico">
                        
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
                  <div class="tab-pane fade" id="tab_1_3">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
   <script type="text/javascript">
   function atendSupport(valorCaja1){
      var parametros = {
         "valorCaja1": valorCaja1
      };
      $.ajax({
         data: parametros,
         url:  'atendSupport',
         type: 'post',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>
   <script type="text/javascript">
   function cargaHistorico(){
     
      $.ajax({
         data: null,
         url:  'adminHistorico',
         type: 'get',

         success: function(response){
               $("#historico").html(response);
         }
      });
   }
   </script>