	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
					Mis Solicitudes
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="javascript:;">Página Principal</a> 
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:;">Help Desk</a>
						<i class="icon-angle-right"></i> 
					</li>
					<li>
						<a href="javascript:;">FAQ</a> 
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
         	<div class="col-md-12">
	            <div class="table-responsive">
	               <div class="tabbable tabbable-custom">
	                    <ul class="nav nav-tabs">
	                    	<li class=""><a href="my_supports">Nueva solicitud</a></li>
	                    	<li class=""><a href="my_supports_pending">Mis solicitudes</a></li>
	                    	<li class=""><a href="my_supports_history">Histórico</a></li>
	                    	<li class="active"><a href="my_supports_faq">FAQ</a></li>
	                  	</ul>
	                  	<div class="tab-content">
	                    	<div class="tab-pane active" id="fqa_supports">
		                        <div class="row">
						            <div class="col-md-12">
						        		<div class="col-md-3">
						        		   <ul class="ver-inline-menu tabbable margin-bottom-10">
						        		      
						        		      @foreach($categories as $category)
						        		      	<li><a data-toggle="tab" href="#tab_{{$category->id}}"><i class="icon-group"></i> {{$category->name}}</a></li>
						        		      @endforeach
						        		   </ul>
						        		</div>
						        		<div class="col-md-9">
						        		   	<div class="tab-content">
						        		   	@foreach($categories as $category)
						        		   	<div id="tab_{{$category->id}}" class="tab-pane">
						        		      	@foreach($support_faqs as $support_faq)
						        		      	@if($category->id == $support_faq->support_category_id)
						        		      	
						        		         	<div id="accordion1" class="panel-group">
						        		            	<div class="panel panel-default">
						        		               		<div class="panel-heading">
						        		                  		<h4 class="panel-title">
						        		                     		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_{{$support_faq->id}}">
						        		                     		{{$support_faq->title}} {{$support_faq->SupportCategory->name}}
						        		                     		</a>
						        		                  		</h4>
						        		               		</div>
						        		               		@if(Input::get('id') == $support_faq->id)

						        		               		<div id="accordion1_{{$support_faq->id}}" class="panel-collapse collapse in">
						        		               		@else
						        		               		<div id="accordion1_{{$support_faq->id}}" class="panel-collapse collapse">
						        		               		@endif
						        		                  		<div class="panel-body">
						        		                   		{{$support_faq->text}}  
						        		                  		</div>
						        		               		</div>
						        		            	</div>
						        		         	</div>
						        		      	@endif
						        		      	

						        		      	@endforeach
						        		   	</div>
						        		   		
						        		   	@endforeach
						        		     
						        		   	</div>
						        		</div>        
									</div>
									<div class="col-md-6">
									    <div class="portlet grey">
									        <div class="portlet-title">
									            <div class="caption"><i class="icon-file"></i>Video</div>
									        </div>
									        <div class="portlet-body">
									            <div class="list-group">
									                <a href="#" class="list-group-item active bg-yellow">Uso del Teléfono IP Básico</a>
									                <video width="100%" height="315" controls>
									                    <source src="{{ URL::to("assets/multimedia/Tel_IP_Basico.mp4")}}" type="video/mp4">
									                    Your browser does not support the video tag.
									                </video>
									            </div>
									        </div>
									    </div>
									</div>
									<div class="col-md-6">
									    <div class="portlet grey">
									        <div class="portlet-title">
									            <div class="caption"><i class="icon-file"></i>Video</div>
									        </div>
									        <div class="portlet-body">
									            <div class="list-group">
									                <a href="#" class="list-group-item active bg-yellow">Uso del Teléfono IP Avanzado</a>
									                <video width="100%" height="315" controls>
									                    <source src="{{ URL::to("assets/multimedia/Tel_IP_Avanzado.mp4")}}" type="video/mp4">
									                    Your browser does not support the video tag.
									                </video>
									            </div>
									        </div>
									    </div>
									</div>
		                        </div>
	                    	</div>   
	                    </div>
	                </div>
	            </div>
         	</div>
        </div>
    </div>
</div>      